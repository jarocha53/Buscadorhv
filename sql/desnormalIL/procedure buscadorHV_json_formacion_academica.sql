use PLavoro
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION buscadorHV_json_formacion_academica
(
	-- Add the parameters for the function here
	@id_persona int
)
RETURNS varchar(max)
AS
BEGIN




	DECLARE @titulo VARCHAR(255)
	DECLARE @nivel_educativo VARCHAR(50)
	DECLARE @nucleo_basico_conocimiento VARCHAR(255)
	DECLARE @fecha_inicio VARCHAR(13)
	DECLARE @fecha_finalizacion VARCHAR(13)
	DECLARE @pais VARCHAR(255)
	DECLARE @departamento VARCHAR(255)
	DECLARE @municipio VARCHAR(255)
	DECLARE @institucion VARCHAR(255)
	DECLARE @estado VARCHAR(255)




	DECLARE @FORMACIONES_ACADEMICAS VARCHAR(MAX)


	SET @FORMACIONES_ACADEMICAS = ' ['


	DECLARE FORMACIONES_ACADEMICAS_CUR CURSOR FOR 
	SELECT TOP 1000 
		ISNULL(TISTUD.TIT_EST_TXT,'')								AS titulo,
		ISNULL((SELECT top 1 TADES.DESCRIZIONE 
			FROM TADES 
			WHERE TADES.NOME_TABELLA LIKE '%LSTUD%' 
			AND TADES.CODICE = TISTUD.COD_LIV_STUD),'')				AS nivel_educativo,
		ISNULL((SELECT top 1 TADES.DESCRIZIONE 
			FROM TADES 
			WHERE TADES.NOME_TABELLA LIKE '%TSTUD%' 
			AND TADES.CODICE = TISTUD.COD_TIT_STUD),'')				AS nucleo_basico_conocimiento,
		''															AS fecha_inicio,
		ISNULL( CONVERT(varchar(12),TISTUD.AA_STUD),'')				AS fecha_finalizacion,
		ISNULL(TISTUD.COD_STAT_FREQ,'')								AS pais,
		''															AS departamento,
		''															AS municipio,
		ISNULL(TISTUD.NOME_ISTITUTO,'')								AS institucion,
		ISNULL(CASE 
			WHEN TISTUD.COD_STAT_STUD = '0' THEN 'GRADUADO'
			WHEN TISTUD.COD_STAT_STUD = '1' THEN 'INCOMPLETO'
			WHEN TISTUD.COD_STAT_STUD = '2' THEN 'EN CURSO'
			WHEN TISTUD.COD_STAT_STUD = '4' THEN 'NO CORRESPONDE'
			ELSE 'N/A'
		END,'')
																	AS estado
	FROM TISTUD where ID_PERSONA = @id_persona
	

	OPEN FORMACIONES_ACADEMICAS_CUR

	FETCH FORMACIONES_ACADEMICAS_CUR INTO @titulo, @nivel_educativo, @nucleo_basico_conocimiento,@fecha_inicio, @fecha_finalizacion, @pais, 
											@departamento, @municipio, @institucion, @estado

	WHILE @@FETCH_STATUS = 0
		BEGIN
			set @FORMACIONES_ACADEMICAS = @FORMACIONES_ACADEMICAS+'{
					"titulo":"'+@titulo+'",
					"nivel_educativo":"'+@nivel_educativo+'",
					"nucleo_basico_conocimiento":"'+@nucleo_basico_conocimiento+'",
					"fecha_inicio":"'+@fecha_inicio+'",
					"fecha_finalizacion":"'+@fecha_finalizacion+'",
					"ubicacion":{
						"pais":"'+@pais+'",
						"departamento":"'+@departamento+'",
						"municipio":"'+@municipio+'"
					},
					"institucion":"'+@institucion+'",
					"estado":"'+@estado+'"
				},'

			FETCH FORMACIONES_ACADEMICAS_CUR INTO @titulo, @nivel_educativo, @nucleo_basico_conocimiento,@fecha_inicio, @fecha_finalizacion, @pais, @departamento, @municipio, @institucion, @estado
		END 
	CLOSE FORMACIONES_ACADEMICAS_CUR
	DEALLOCATE FORMACIONES_ACADEMICAS_CUR
	set @FORMACIONES_ACADEMICAS = @FORMACIONES_ACADEMICAS + ']'

	return @FORMACIONES_ACADEMICAS

END
GO

