use PLavoro
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION buscadorHV_json_idiomas
(
	-- Add the parameters for the function here
	@id_persona int
)
RETURNS varchar(max)
AS
BEGIN



	DECLARE @nombre VARCHAR(255)
	DECLARE @nivel VARCHAR(50)





	DECLARE @IDIOMAS VARCHAR(1000)


	SET @IDIOMAS = ' ['


	DECLARE IDIOMAS_CUR CURSOR FOR 
	SELECT TOP 10000 CONOSCENZE.DENOMINAZIONE nombre,
		CASE 
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 0 THEN 'Muy Bueno'
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 1 THEN 'Bueno'
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 2 THEN 'Regular'
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 3 THEN 'Basico'
			ELSE 'N/A'
		END as nivel
		from PERS_CONOSC
		INNER JOIN CONOSCENZE on CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA
		INNER JOIN AREA_CONOSCENZA on CONOSCENZE.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA
		where AREA_CONOSCENZA.ID_AREACONOSCENZA = 23 and  PERS_CONOSC.ID_PERSONA = @id_persona
	

	OPEN IDIOMAS_CUR

	FETCH IDIOMAS_CUR INTO @nombre, @nivel

	WHILE @@FETCH_STATUS = 0
		BEGIN
			set @IDIOMAS = @IDIOMAS+'{
					"nombre":"'+@nombre+'",
					"nivel":"'+@nivel+'"
				},'

			FETCH IDIOMAS_CUR INTO @nombre, @nivel
	END 
	CLOSE IDIOMAS_CUR
	DEALLOCATE IDIOMAS_CUR
	set @IDIOMAS = @IDIOMAS + ']'

	return @IDIOMAS

END
GO

 