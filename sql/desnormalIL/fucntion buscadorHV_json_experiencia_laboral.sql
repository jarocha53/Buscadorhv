use PLavoro
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[buscadorHV_json_experiencia_laboral]
(
	-- Add the parameters for the function here
	@id_persona int
)
RETURNS VARCHAR(MAX)
AS
BEGIN


DECLARE @cargo VARCHAR(250)
DECLARE @funciones VARCHAR(1000)
DECLARE @remuneracion_salarial_mensual VARCHAR(1)
DECLARE @tipo_vinculacion VARCHAR(50)
DECLARE @empresa VARCHAR(50)
DECLARE @pais VARCHAR(50)
DECLARE @departamento VARCHAR(50)
DECLARE @municipio VARCHAR(50)
DECLARE @fecha_ingreso VARCHAR(13)
DECLARE @fecha_finalizacion VARCHAR(13)




	DECLARE @ESPERIENCIAS_LABORALES VARCHAR(MAX)

	

	SET @ESPERIENCIAS_LABORALES = ' ['


	DECLARE ESPERIENCIAS_LABORALES_CUR CURSOR FOR 
	SELECT
		PLavoro.dbo.ESPRO.CARGO_ASOC AS cargo, 
		ISNULL(PLavoro.dbo.ESPRO.TAREASREALIZADAS,'')+' - '+ISNULL(PLavoro.dbo.ESPRO.TAREAS,'') AS funciones,
		'' AS remuneracion_salarial_mensual,
		 ISNULL((select TOP 1 DESCRIZIONE from tades where nome_tabella = 'TESPR'
		AND CODICE = PLavoro.dbo.ESPRO.COD_ESPERIENZA),'') AS tipo_vinculacion,
		PLavoro.dbo.ESPRO.RAG_SOC AS empresa,
		'' as pais,
		'' as departamento,
		'' as municipio,
		CONVERT(varchar(11),PLavoro.dbo.ESPRO.DT_INI_ESP_PRO) as fecha_ingreso,
		ISNULL(CONVERT(varchar(11),PLavoro.dbo.ESPRO.DT_FIN_ESP_PRO),'presente') as fecha_finalizacion

		FROM PLavoro.dbo.ESPRO
		WHERE PLavoro.dbo.ESPRO.ID_PERSONA = @id_persona
	





	OPEN ESPERIENCIAS_LABORALES_CUR

	FETCH ESPERIENCIAS_LABORALES_CUR INTO @cargo, @funciones, @remuneracion_salarial_mensual, 
	@tipo_vinculacion, @empresa, @pais, @departamento, @municipio, @fecha_ingreso, @fecha_finalizacion

	WHILE @@FETCH_STATUS = 0
		BEGIN
			set @ESPERIENCIAS_LABORALES = @ESPERIENCIAS_LABORALES+'{
					"cargo":"'+ISNULL(@cargo,'')+'",
					"funciones":"'+ISNULL(@funciones,'')+'",
					"remuneracion_salarial_mensual":"'+ISNULL(@remuneracion_salarial_mensual,'')+'",
					"tipo_vinculacion":"'+ISNULL(@tipo_vinculacion,'')+'",
					"empresa":"'+ISNULL(@empresa,'')+'",
					"ubicacion":{
						"pais":"'+ISNULL(@pais,'')+'",
						"departamento":"'+ISNULL(@departamento,'')+'",
						"municipio":"'+ISNULL(@municipio,'')+'"
					},
					"fecha_ingreso":"'+ISNULL(@fecha_ingreso,'')+'",
					"fecha_finalizacion":"'+ISNULL(@fecha_finalizacion,'')+'"
				},'

			FETCH ESPERIENCIAS_LABORALES_CUR INTO @cargo, @funciones, @remuneracion_salarial_mensual, @tipo_vinculacion, @empresa, @pais, @departamento, @municipio, @fecha_ingreso, @fecha_finalizacion
		END 
	CLOSE ESPERIENCIAS_LABORALES_CUR
	DEALLOCATE ESPERIENCIAS_LABORALES_CUR
	set @ESPERIENCIAS_LABORALES = @ESPERIENCIAS_LABORALES + ']'

	return @ESPERIENCIAS_LABORALES

END
GO

