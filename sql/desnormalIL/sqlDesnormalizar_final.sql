use PLavoro


--select * from PERSONAS_JSON
--REPLACE(@string, CHAR(13), '')
--truncate table PLavoro.dbo.PERSONAS_JSON
INSERT INTO PLavoro.dbo.PERSONAS_JSON
SELECT

--top 10000

DISTINCT dbo.PERSONA.ID_PERSONA id_persona,
isnull(dbo.PERSONA.LOC_RES,0) 	AS ciudad,
PLavoro.dbo.buscadorHV_json_maximo_nivel_educativo_by_id_persona(dbo.PERSONA.ID_PERSONA) as nivel_educativo,
isnull(dbo.PERSONAS_JOB_PROFILE.MONTHLYSALARYALLOWANCE,0) as aspiracion_salarial,
REPLACE(REPLACE(REPLACE(REPLACE('{
		"persona":{
			
			"informacion_personal":{
				"fecha_nacimiento":"'+CONVERT(VARCHAR(11),dbo.PERSONA.DT_NASC,106)+'",
				"nombre":"'+dbo.PERSONA.NOME +' '+ dbo.PERSONA.SECONDO_NOME+'",
				"primer_apellido":"'+dbo.PERSONA.COGNOME+'",
				"segundo_apellido":"'+dbo.PERSONA.SECONDO_COGNOME+'",
				"sexo":"'+dbo.PERSONA.SESSO+'",
				"tipo_documento":"'+ SUBSTRING(dbo.PERSONA.COD_FISC, 0,3)+'",
				"numero_documento":"'+ SUBSTRING(dbo.PERSONA.COD_FISC, 3,20)+'",
				"libreta_militar":{
					"tiene?":"'+ ISNULL((case WHEN dbo.PERSONA.TIPO_LIBRETA_MILITAR = 3 THEN 'no' else 'si' end),'') +'",
					"categoria":"'+ ISNULL((select descrizione from tades where nome_tabella = 'PTLM' and codice = dbo.PERSONA.TIPO_LIBRETA_MILITAR),'')+'"
				},
				"ubicacion_nacimiento":{
					"pais":"'+ISNULL(dbo.PERSONA.STAT_NASC,'')+'",
					"departamento":"'+ISNULL(dbo.PERSONA.PRV_NASC,'')+'",
					"municipio":"'+ISNULL(dbo.PERSONA.COM_NASC,'')+'",
					"centro_poblado":"'+ISNULL((case when dbo.PERSONA.COM_NASC <> '' then  dbo.PERSONA.COM_NASC+'01' else '' END),'')+'"
				},
				"estado_civil":"'+ISNULL((select descrizione from tades where nome_tabella = 'STCIV' and codice = dbo.PERSONA.STAT_CIV),'')+'",
				"nacionalidad":[
					{"pais":"'+ISNULL(dbo.PERSONA.STAT_NASC,'')+'"}
				],
				"es_jefe_de_hogar":"'+ISNULL((CASE when dbo.PERSONA.JEFE_HOGAR=1 then 'si' else 'no' END),'')+'",
				"catidad_personas_cargo":"'+ ISNULL((SELECT convert(varchar(11),count(1)) as personas FROM PERS_FAMILIARI
where  PERS_FAMILIARI.fl_carico = 'S' and PERS_FAMILIARI.id_persona = dbo.PERSONA.ID_PERSONA
GROUP BY PERS_FAMILIARI.id_persona),0)+
			'"},
			"informacion_contacto":{
				"residencia":{
					"pais":"'+ISNULL(dbo.PERSONA.STAT_RES,'')+'",
					"departamento":"'+ISNULL(CAST(dbo.PERSONA.DEP_RES  AS NVARCHAR),'')+'",
					"municipio":"'+ISNULL(CAST(dbo.PERSONA.LOC_RES  AS NVARCHAR),'')+'",
					"centro_poblado":"'+ISNULL(CAST(dbo.PERSONA.LOC_RES  AS NVARCHAR),'')+'01'+'",
					"barrio":"'+ISNULL(dbo.PERSONA.FRAZIONE_RES,'')+'",
					"direccion":"'+ISNULL(dbo.PERSONA.IND_RES,'')+'"
				},
				"telefono":[{"numero":"'+ISNULL(dbo.PERSONA.NUM_TEL,'')+'"}],
				"correo_electronico":"'+ISNULL(dbo.PERSONA.E_MAIL,'')+'"
			},
			"informacion_laboral":{
				"descripcion_perfil_laboral":"'+REPLACE(ISNULL(dbo.PERSONAS_JOB_PROFILE.JB_PROFILE,''), CHAR(13), '')+'",
				"anhos_experiencia":"",
				"aspiracion_salarial":"'+ISNULL(CAST(dbo.PERSONAS_JOB_PROFILE.MONTHLYSALARYALLOWANCE AS NVARCHAR),'')+'",
				"disponibilidad_viajar":"",
				"disponibilidad_cambio_domicilio_nacional":"",
				"disponibilidad_cambio_domicilio_internacional":"",
				"intereses_ocupacionales":'+ [PLavoro].[dbo].[buscadorHV_json_interesesOcupacionales](dbo.PERSONA.ID_PERSONA)+'
			},
			"informacion_adicional":{
				"licencia_conduccion":"'+ISNULL((
				select Descrizione from tades where nome_tabella = 'TPERM' and codice = (select top 1 COD_PERM from AUTCERT
where ID_PERSONA = dbo.PERSONA.ID_PERSONA)),'n/a')+'",
				"tipo_discapacidad":"'+ISNULL((select descrizione from tades  where nome_tabella = 'TCAPI' and codice = (select top 1 cod_catprot from PERS_CATPROT where id_persona = dbo.PERSONA.ID_PERSONA)),'Ninguna')+'"
			},
			"experiencia_laboral":'+PLavoro.dbo.buscadorHV_json_experiencia_laboral(dbo.PERSONA.ID_PERSONA)+',
			"formacion_academica":'+PLavoro.dbo.buscadorHV_json_formacion_academica(dbo.PERSONA.ID_PERSONA)+',
			"cursos_capacitacion":'+PLavoro.dbo.buscadorHV_json_cursos_capacitacion(dbo.PERSONA.ID_PERSONA)+',
			"idiomas":'+PLavoro.dbo.buscadorHV_json_idiomas(dbo.PERSONA.ID_PERSONA)+',
			"conocimientos_informaticos":'+PLavoro.dbo.buscadorHV_json_conocimientos_informaticos(dbo.PERSONA.ID_PERSONA)+'

		}
	}' ,CHAR(9),' '),CHAR(10),' '),CHAR(13),' '),'},]','}]') as hv
	--into PERSONAS_JSON
FROM
dbo.PERSONA
INNER JOIN dbo.PERSONAS_JOB_PROFILE ON dbo.PERSONAS_JOB_PROFILE.ID_PERSONA = dbo.PERSONA.ID_PERSONA
--WHERE 
--dbo.persona.ID_PERSONA = 187379
--1 != 1
--dbo.PERSONA.COD_FISC = 'CC1116252550'
