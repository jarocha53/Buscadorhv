use PLavoro
SELECT
top 10000
'{
		"persona":{
			
			"informacion_personal":{
				"fecha_nacimiento":"'+CONVERT(VARCHAR(11),dbo.PERSONA.DT_NASC,106)+'",
				"nombre":"'+dbo.PERSONA.NOME +' '+ dbo.PERSONA.SECONDO_NOME+'",
				"primer_apellido":"'+dbo.PERSONA.COGNOME+'",
				"segundo_apellido":"'+dbo.PERSONA.SECONDO_COGNOME+'",
				"sexo":"'+dbo.PERSONA.SESSO+'",
				"tipo_documento":"'+ SUBSTRING(dbo.PERSONA.COD_FISC, 0,3)+'",
				"numero_documento":"'+ SUBSTRING(dbo.PERSONA.COD_FISC, 3,20)+'",
				"libreta_militar":{
					"tiene?":"'+ ISNULL((case WHEN dbo.PERSONA.TIPO_LIBRETA_MILITAR = 3 THEN 'no' else 'si' end),'') +'",
					"categoria":"'+ ISNULL((select descrizione from tades where nome_tabella = 'PTLM' and codice = dbo.PERSONA.TIPO_LIBRETA_MILITAR),'')+'"
				},
				"ubicacion_nacimiento":{
					"pais":"'+ISNULL(dbo.PERSONA.STAT_NASC,'')+'",
					"departamento":"'+ISNULL(dbo.PERSONA.PRV_NASC,'')+'",
					"municipio":"'+ISNULL(dbo.PERSONA.COM_NASC,'')+'",
					"centro_poblado":"'+ISNULL((case when dbo.PERSONA.COM_NASC <> '' then  dbo.PERSONA.COM_NASC+'01' else '' END),'')+'"
				},
				"estado_civil":"'+ISNULL((select descrizione from tades where nome_tabella = 'STCIV' and codice = dbo.PERSONA.STAT_CIV),'')+'",
				"nacionalidad":[
					{"pais":"'+ISNULL(dbo.PERSONA.STAT_NASC,'')+'"}
				],
				"es_jefe_de_hogar":"'+ISNULL((CASE when dbo.PERSONA.JEFE_HOGAR=1 then 'si' else 'no' END),'')+'",
				"catidad_personas_cargo":"'+ ISNULL((SELECT convert(varchar(11),count(1)) as personas FROM PERS_FAMILIARI
where  PERS_FAMILIARI.fl_carico = 'S' and PERS_FAMILIARI.id_persona = dbo.PERSONA.ID_PERSONA
GROUP BY PERS_FAMILIARI.id_persona),0)+
			'"},
			"informacion_contacto":{
				"residencia":{
					"pais":"'+ISNULL(dbo.PERSONA.STAT_RES,'')+'",
					"departamento":"'+ISNULL(CAST(dbo.PERSONA.DEP_RES  AS NVARCHAR),'')+'",
					"municipio":"'+ISNULL(CAST(dbo.PERSONA.LOC_RES  AS NVARCHAR),'')+'",
					"centro_poblado":"'+ISNULL(CAST(dbo.PERSONA.LOC_RES  AS NVARCHAR),'')+'01'+'",
					"barrio":"'+ISNULL(dbo.PERSONA.FRAZIONE_RES,'')+'",
					"direccion":"'+ISNULL(dbo.PERSONA.IND_RES,'')+'"
				},
				"telefono":[{"numero":"'+ISNULL(dbo.PERSONA.NUM_TEL,'')+'"}],
				"correo_electronico":"'+ISNULL(dbo.PERSONA.E_MAIL,'')+'"
			},
			"informacion_laboral":{
				"descripcion_perfil_laboral":"'+ISNULL(dbo.PERSONAS_JOB_PROFILE.JB_PROFILE,'')+'",
				"anhos_experiencia":"",
				"aspiracion_salarial":"'+ISNULL(CAST(dbo.PERSONAS_JOB_PROFILE.MONTHLYSALARYALLOWANCE AS NVARCHAR),'')+'",
				"disponibilidad_viajar":"",
				"disponibilidad_cambio_domicilio_nacional":"",
				"disponibilidad_cambio_domicilio_internacional":"",
				"intereses_ocupacionales":'+ [PLavoro].[dbo].[buscadorHV_json_interesesOcupacionales](dbo.PERSONA.ID_PERSONA)+'
			},
			"informacion_adicional":{
				"licencia_conduccion":"'+ISNULL((
				select Descrizione from tades where nome_tabella = 'TPERM' and codice = (select top 1 COD_PERM from AUTCERT
where ID_PERSONA = dbo.PERSONA.ID_PERSONA)),'n/a')+'",
				"tipo_discapacidad":"'+ISNULL((select descrizione from tades  where nome_tabella = 'TCAPI' and codice = (select top 1 cod_catprot from PERS_CATPROT where id_persona = dbo.PERSONA.ID_PERSONA)),'Ninguna')+'"
			},
			"experiencia_laboral":'+PLavoro.dbo.buscadorHV_json_experiencia_laboral(dbo.PERSONA.ID_PERSONA)+',
			"formacion_academica":'+PLavoro.dbo.buscadorHV_json_formacion_academica(dbo.PERSONA.ID_PERSONA)+',
			"cursos_capacitacion":'+PLavoro.dbo.buscadorHV_json_cursos_capacitacion(dbo.PERSONA.ID_PERSONA)+',
			"idiomas":'+PLavoro.dbo.buscadorHV_json_idiomas(dbo.PERSONA.ID_PERSONA)+',
			"conocimientos_informaticos":'+PLavoro.dbo.buscadorHV_json_conocimientos_informaticos(dbo.PERSONA.ID_PERSONA)+'

		}
	}' as json,
dbo.PERSONA.DT_NASC as fecha_nacimiento,
dbo.PERSONA.NOME +' '+ dbo.PERSONA.SECONDO_NOME as nombre,
dbo.PERSONA.COGNOME as primer_apellido,
dbo.PERSONA.SECONDO_COGNOME as segundo_apellido,
dbo.PERSONA.SESSO AS sexo,
SUBSTRING(dbo.PERSONA.COD_FISC, 0,3)as tipo_documento,
SUBSTRING(dbo.PERSONA.COD_FISC, 3,20) as numero_documento,
case WHEN dbo.PERSONA.TIPO_LIBRETA_MILITAR = 3 THEN 'no' else 'si' end as libreta_militar_tiene,
ISNULL((select descrizione from tades where nome_tabella = 'PTLM' and codice = dbo.PERSONA.TIPO_LIBRETA_MILITAR),'') as libreta_militar_categoria,
dbo.PERSONA.TIPO_LIBRETA_MILITAR,
dbo.PERSONA.STAT_NASC as ubicacion_nacimiento_pais,
dbo.PERSONA.PRV_NASC as ubicacion_nacimiento_departamento,
dbo.PERSONA.COM_NASC as ubicacion_nacimiento_ciudad,
case when dbo.PERSONA.COM_NASC <> '' then  dbo.PERSONA.COM_NASC+'01' else '' END as ubicacion_nacimiento_centro_poblado,
(select descrizione from tades where nome_tabella = 'STCIV' and codice = dbo.PERSONA.STAT_CIV)  as estado_civil,
dbo.PERSONA.STAT_NASC as nacionalidad,
dbo.PERSONA.JEFE_HOGAR AS es_jefe_de_hogar,
ISNULL((SELECT count(1) as personas FROM PERS_FAMILIARI 
where  PERS_FAMILIARI.fl_carico = 'S' and PERS_FAMILIARI.id_persona = dbo.PERSONA.ID_PERSONA
GROUP BY PERS_FAMILIARI.id_persona),0) as catidad_personas_cargo,
dbo.PERSONA.STAT_RES 	AS informacion_contacto_residencia_pais,
dbo.PERSONA.DEP_RES 	AS informacion_contacto_residencia_departamento,
dbo.PERSONA.LOC_RES 	AS informacion_contacto_residencia_municipio,
dbo.PERSONA.LOC_RES+'01' AS informacion_contacto_residencia_centro_poblado,
dbo.PERSONA.FRAZIONE_RES as informacion_contacto_residencia_barrio,
dbo.PERSONA.IND_RES as informacion_contacto_residencia_direccion,
dbo.PERSONA.NUM_TEL as informacion_contacto_telefono,
dbo.PERSONA.E_MAIL as informacion_contacto_correo_electronico,
dbo.PERSONAS_JOB_PROFILE.JB_PROFILE as informacion_laboral_descripcion_perfil_laboral,
'' as informacion_laboral_anhos_experiencia,
dbo.PERSONAS_JOB_PROFILE.MONTHLYSALARYALLOWANCE as informacion_laboral_aspiracion_salarial,
'' as informacion_laboral_disponibilidad_viajar,
'' as informacion_laboral_disponibilidad_cambio_domicilio_nacional,
'' as informacion_laboral_disponibilidad_cambio_domicilio_internacional,
'' as informacion_laboral_intereses_ocupacionales
--dbo.PERSONAS_JOB_PROFILE.JOB_PROFILE as informacion_laboral_descripcion_perfil_laboral,
FROM
dbo.PERSONA
INNER JOIN dbo.PERSONAS_JOB_PROFILE ON dbo.PERSONAS_JOB_PROFILE.ID_PERSONA = dbo.PERSONA.ID_PERSONA
--WHERE
--dbo.PERSONA.COD_FISC = 'CC1116252550'
