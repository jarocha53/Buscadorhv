use PLavoro
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION buscadorHV_json_cursos_capacitacion
(
	-- Add the parameters for the function here
	@id_persona int
)
RETURNS VARCHAR(MAX)
AS
BEGIN


DECLARE @nombre						VARCHAR(250)
DECLARE @nucleo_basico_conocimiento VARCHAR(100)
DECLARE @fecha_inicio				VARCHAR(50)
DECLARE @fecha_finalizacion			VARCHAR(50)
DECLARE @duracion					VARCHAR(50)
DECLARE @institucion				VARCHAR(50)
DECLARE @tipo_curso					VARCHAR(50)
DECLARE @estado						VARCHAR(50)



	DECLARE @CURSOS_CAPACITACION VARCHAR(MAX)

	

	SET @CURSOS_CAPACITACION = ' ['


	DECLARE CURSOS_CAPACITACION_CUR CURSOR FOR 
	SELECT
		ISNULL(dbo.FORMAZIONE.COD_TCORSO,'')								AS nombre,
		ISNULL(dbo.AREA_CORSO.DENOMINAZIONE	,'')									AS nucleo_basico_conocimiento,
		ISNULL(CONVERT(VARCHAR(13), dbo.FORMAZIONE.DTINICIO)	,'')											AS fecha_inicio,
		ISNULL(CONVERT(VARCHAR(13),dbo.FORMAZIONE.DTFIN)	,'')											AS fecha_finalizacion,
		ISNULL(dbo.FORMAZIONE.ORE_DURATA,'')											AS duracion,
		ISNULL(dbo.FORMAZIONE.DESC_ENTE	,'')										AS institucion,
		ISNULL(CASE -- DIZ_DATI - ID_TAB = FORMAZIONE AND ID_CAMPO = COD_ATT_FINALE --ID_CAMPO_DESC = N|Diplomado|C|Seminario|F|Curso|Q|Otro|S|Taller|T|Certificación
		 WHEN dbo.FORMAZIONE.COD_ATT_FINALE = 'N' THEN 'Diplomado'
		 WHEN dbo.FORMAZIONE.COD_ATT_FINALE = 'C' THEN 'Seminario'
		 WHEN dbo.FORMAZIONE.COD_ATT_FINALE = 'F' THEN 'Curso'
		 WHEN dbo.FORMAZIONE.COD_ATT_FINALE = 'S' THEN 'Taller'
		 WHEN dbo.FORMAZIONE.COD_ATT_FINALE = 'T' THEN 'Certificación'
		 ELSE 'Otro' -- 'Q' Y LO DEMÁS Q VENGA
		END	,'')																AS tipo_curso,
		ISNULL(CASE -- DIZ_DATI - ID_TAB = FORMAZIONE AND ID_CAMPO = COD_STATUS_CORSO --ID_CAMPO_DESC = 0|Finalizado|1|Incompleto|2|En curso
		 WHEN  dbo.FORMAZIONE.COD_STATUS_CORSO = '0' THEN 'Finalizado'
		 WHEN  dbo.FORMAZIONE.COD_STATUS_CORSO = '1' THEN 'Incompleto'
		 WHEN  dbo.FORMAZIONE.COD_STATUS_CORSO = '2' THEN 'En curso'
		 ELSE 'N/A'
		END	,'')																AS estado
		FROM
		dbo.FORMAZIONE
		INNER JOIN dbo.AREA_CORSO ON dbo.FORMAZIONE.ID_AREA = dbo.AREA_CORSO.ID_AREA
		WHERE dbo.FORMAZIONE.ID_PERSONA = @id_persona
	





	OPEN CURSOS_CAPACITACION_CUR

	FETCH CURSOS_CAPACITACION_CUR INTO @nombre, @nucleo_basico_conocimiento, @fecha_inicio, @fecha_finalizacion, @duracion, @institucion, @tipo_curso, @estado

	WHILE @@FETCH_STATUS = 0
		BEGIN
			set @CURSOS_CAPACITACION = @CURSOS_CAPACITACION+'{
					"nombre":"'+@nombre+'",
					"nucleo_basico_conocimiento":"'+@nucleo_basico_conocimiento+'",
					"fecha_inicio":"'+@fecha_inicio+'",
					"fecha_finalizacion":"'+@fecha_finalizacion+'",
					"duracion":"'+@duracion+'",
					"ubicacion":{
						"pais":"",
						"departamento":"",
						"municipio":""
					},
					"institucion":"'+@institucion+'",
					"tipo_curso":"'+@tipo_curso+'",
					"estado":"'+@estado+'"
				},'

			FETCH CURSOS_CAPACITACION_CUR INTO @nombre, @nucleo_basico_conocimiento, @fecha_inicio, @fecha_finalizacion, @duracion, @institucion, @tipo_curso, @estado
		END 
	CLOSE CURSOS_CAPACITACION_CUR
	DEALLOCATE CURSOS_CAPACITACION_CUR
	set @CURSOS_CAPACITACION = @CURSOS_CAPACITACION + ']'

	return @CURSOS_CAPACITACION


END
GO

