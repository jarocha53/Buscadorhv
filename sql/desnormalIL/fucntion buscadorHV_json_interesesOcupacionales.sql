use PLavoro
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION buscadorHV_json_interesesOcupacionales
(
	@id_persona int
)
RETURNS varchar
AS
BEGIN
	
DECLARE @denominazione VARCHAR(50)
	DECLARE @INTERESES_OCUPACIONALES VARCHAR(max)

	set @id_persona = 2002649

	SET @INTERESES_OCUPACIONALES = ' ['


	DECLARE INTERESES_OCUPACIONALES_CUR CURSOR FOR 
	SELECT ISNULL(denominazione,'') as denominazione FROM PERS_FIGPROF 
	INNER JOIN FIGUREPROFESSIONALI ON PERS_FIGPROF.ID_FIGPROF = FIGUREPROFESSIONALI.ID_FIGPROF
	WHERE PERS_FIGPROF.ID_PERSONA = @id_persona

	OPEN INTERESES_OCUPACIONALES_CUR

	FETCH INTERESES_OCUPACIONALES_CUR INTO @denominazione

	WHILE @@FETCH_STATUS = 0
		BEGIN
			set @INTERESES_OCUPACIONALES = @INTERESES_OCUPACIONALES+'{"interes":"'+@denominazione+'"},'

			FETCH INTERESES_OCUPACIONALES_CUR INTO @denominazione
		END 
	CLOSE INTERESES_OCUPACIONALES_CUR
	DEALLOCATE INTERESES_OCUPACIONALES_CUR
	set @INTERESES_OCUPACIONALES = @INTERESES_OCUPACIONALES + ']'

	return @INTERESES_OCUPACIONALES

END
GO

