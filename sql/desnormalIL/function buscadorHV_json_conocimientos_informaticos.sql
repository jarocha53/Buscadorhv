use PLavoro
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION buscadorHV_json_conocimientos_informaticos
(
	-- Add the parameters for the function here
	@id_persona int
)
RETURNS varchar(max)
AS
BEGIN



	DECLARE @nombre VARCHAR(255)
	DECLARE @nivel VARCHAR(50)





	DECLARE @CONOCIMIENTOS_INFORMATICOS VARCHAR(1000)


	SET @CONOCIMIENTOS_INFORMATICOS = ' ['


	DECLARE CONOCIMIENTOS_INFORMATICOS_CUR CURSOR FOR 
	SELECT TOP 10000 CONOSCENZE.DENOMINAZIONE nombre,
		CASE 
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 0 THEN 'Muy Bueno'
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 1 THEN 'Bueno'
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 2 THEN 'Regular'
			WHEN PERS_CONOSC.COD_GRADO_CONOSC = 3 THEN 'Basico'
			ELSE 'N/A'
		END as nivel
		from PERS_CONOSC
		INNER JOIN CONOSCENZE on CONOSCENZE.ID_CONOSCENZA = PERS_CONOSC.ID_CONOSCENZA
		INNER JOIN AREA_CONOSCENZA on CONOSCENZE.ID_AREACONOSCENZA = AREA_CONOSCENZA.ID_AREACONOSCENZA
		where AREA_CONOSCENZA.ID_AREACONOSCENZA in (19, 1004, 1008, 1009, 1011, 1012, 1013, 1014, 1015, 1016)
			AND  PERS_CONOSC.ID_PERSONA = @id_persona
	

	OPEN CONOCIMIENTOS_INFORMATICOS_CUR

	FETCH CONOCIMIENTOS_INFORMATICOS_CUR INTO @nombre, @nivel

	WHILE @@FETCH_STATUS = 0
		BEGIN
			set @CONOCIMIENTOS_INFORMATICOS = @CONOCIMIENTOS_INFORMATICOS+'{
					"nombre":"'+@nombre+'",
					"nivel":"'+@nivel+'"
				},'

			FETCH CONOCIMIENTOS_INFORMATICOS_CUR INTO @nombre, @nivel
	END 
	CLOSE CONOCIMIENTOS_INFORMATICOS_CUR
	DEALLOCATE CONOCIMIENTOS_INFORMATICOS_CUR
	set @CONOCIMIENTOS_INFORMATICOS = @CONOCIMIENTOS_INFORMATICOS + ']'

	return @CONOCIMIENTOS_INFORMATICOS

END
GO