use PLavoro
-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION buscadorHV_json_maximo_nivel_educativo_by_id_persona
(
	-- Add the parameters for the function here
	@ID_PERSONA int
)


RETURNS VARCHAR(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultVar VARCHAR(max);
	

/**
*
NLE	NINGUNO											0
SLE	PREESCOLAR										1
PRI	B�SICA PRIMARIA (1� - 5�) 						2
SEC	B�SICA SECUNDARIA (6� - 9�)						3
MED	MEDIA (10� -13�)								4
TLA	T�CNICA LABORAL									5
CBU	T�CNICA PROFESIONAL								6
POL	TECNOL�GICA										7
NBC	UNIVERSITARIA									8
ESP	ESPECIALIZACI�N									9
POS	POSTGRADO										10
MAE	MAESTR�A										11
DOC	DOCTORADO										12
*
**/

SELECT 
		@ResultVar = ISNULL(CASE 
					WHEN CAT_NIVEL_EDUCATIVO = 0 THEN  'NINGUNO'
					WHEN CAT_NIVEL_EDUCATIVO = 1 THEN  'PREESCOLAR'
					WHEN CAT_NIVEL_EDUCATIVO = 2 THEN  'B�SICA PRIMARIA (1� - 5�)'
					WHEN CAT_NIVEL_EDUCATIVO = 3 THEN  'B�SICA SECUNDARIA (6� - 9�)'
					WHEN CAT_NIVEL_EDUCATIVO = 4 THEN  'MEDIA (10� -13�)'
					WHEN CAT_NIVEL_EDUCATIVO = 5 THEN  'T�CNICA LABORAL'
					WHEN CAT_NIVEL_EDUCATIVO = 6 THEN  'T�CNICA PROFESIONAL'
					WHEN CAT_NIVEL_EDUCATIVO = 7 THEN  'TECNOL�GICA'
					WHEN CAT_NIVEL_EDUCATIVO = 8 THEN  'UNIVERSITARIA'
					WHEN CAT_NIVEL_EDUCATIVO = 9 THEN  'ESPECIALIZACI�N'
					WHEN CAT_NIVEL_EDUCATIVO = 10 THEN 'POSTGRADO'
					WHEN CAT_NIVEL_EDUCATIVO = 11 THEN 'MAESTR�A'
					WHEN CAT_NIVEL_EDUCATIVO = 12 THEN 'DOCTORADO'
					ELSE 'NINGUNO'
			END, 'NINGUNO') 
  FROM (
			SELECT
				(
					SELECT 
						ISNULL(Max(CASE
							WHEN TISTUD.COD_LIV_STUD = 'NLE' THEN 0
							WHEN TISTUD.COD_LIV_STUD = 'SLE' THEN 1
							WHEN TISTUD.COD_LIV_STUD = 'PRI' THEN 2
							WHEN TISTUD.COD_LIV_STUD = 'SEC' THEN 3
							WHEN TISTUD.COD_LIV_STUD = 'MED' THEN 4
							WHEN TISTUD.COD_LIV_STUD = 'TLA' THEN 5
							WHEN TISTUD.COD_LIV_STUD = 'CBU' THEN 6
							WHEN TISTUD.COD_LIV_STUD = 'POL' THEN 7
							WHEN TISTUD.COD_LIV_STUD = 'NBC' THEN 8
							WHEN TISTUD.COD_LIV_STUD = 'ESP' THEN 9
							WHEN TISTUD.COD_LIV_STUD = 'POS' THEN 10
							WHEN TISTUD.COD_LIV_STUD = 'MAE' THEN 11
							WHEN TISTUD.COD_LIV_STUD = 'DOC' THEN 12
						ELSE 0
				END),0) AS CAT_NIVEL_EDUCATIVO
					FROM TISTUD
					WHERE TISTUD.ID_PERSONA = P.ID_PERSONA
					GROUP BY TISTUD.ID_PERSONA
				) AS CAT_NIVEL_EDUCATIVO
			FROM PERSONA P 
WHERE P.ID_PERSONA = @ID_PERSONA
) AS R


	return @ResultVar

END
GO

