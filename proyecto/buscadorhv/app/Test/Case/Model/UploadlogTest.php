<?php
App::uses('Uploadlog', 'Model');

/**
 * Uploadlog Test Case
 *
 */
class UploadlogTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.uploadlog',
		'app.upload'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Uploadlog = ClassRegistry::init('Uploadlog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Uploadlog);

		parent::tearDown();
	}

}
