<?php
App::uses('Resume', 'Model');

/**
 * Resume Test Case
 *
 */
class ResumeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.resume',
		'app.city',
		'app.educationallevel',
		'app.salaryrange',
		'app.resumecheck',
		'app.user',
		'app.group',
		'app.search',
		'app.platform',
		'app.upload'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Resume = ClassRegistry::init('Resume');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Resume);

		parent::tearDown();
	}

}
