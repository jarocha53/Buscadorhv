<?php
App::uses('Platform', 'Model');

/**
 * Platform Test Case
 *
 */
class PlatformTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.platform',
		'app.search',
		'app.user',
		'app.group',
		'app.resumecheck',
		'app.resume',
		'app.city',
		'app.educationallevel',
		'app.salaryrange',
		'app.upload',
		'app.uploadlog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Platform = ClassRegistry::init('Platform');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Platform);

		parent::tearDown();
	}

}
