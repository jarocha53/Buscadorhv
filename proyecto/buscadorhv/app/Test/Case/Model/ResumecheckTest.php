<?php
App::uses('Resumecheck', 'Model');

/**
 * Resumecheck Test Case
 *
 */
class ResumecheckTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.resumecheck',
		'app.resume',
		'app.user',
		'app.group',
		'app.search'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Resumecheck = ClassRegistry::init('Resumecheck');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Resumecheck);

		parent::tearDown();
	}

}
