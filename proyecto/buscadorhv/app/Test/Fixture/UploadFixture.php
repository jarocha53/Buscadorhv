<?php
/**
 * UploadFixture
 *
 */
class UploadFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'user_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'platform_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'file_name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => 'nombre del archivo recibido', 'charset' => 'utf8'),
		'size' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'tamaño total del archivo'),
		'records' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'numero de registros enviados'),
		'errors' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'numero de registros con error al procesar'),
		'susses' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'numero de registros procesados correctamente'),
		'part_file' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'posicion del archivo enviado de la coleccion de archivos que completan el total de hojas de vida enviadas por la plataforma'),
		'total_files' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'total  de archivos que conformaman el conjunto de  hojas de vida enviadas por la plataforma'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'user_id' => array('column' => 'user_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '539f6029-4360-44bf-8932-0198780fb9bf',
			'user_id' => 1,
			'platform_id' => 1,
			'file_name' => 'Lorem ipsum dolor sit amet',
			'size' => 1,
			'records' => 1,
			'errors' => 1,
			'susses' => 1,
			'part_file' => 1,
			'total_files' => 1,
			'created' => '2014-06-16 21:22:49',
			'modified' => '2014-06-16 21:22:49'
		),
	);

}
