<?php
/**
 * ResumeFixture
 *
 */
class ResumeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 36, 'key' => 'primary', 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'city_id' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 7, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'educationallevel_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'salaryrange_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'show_on_queries' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'fulltext' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '539f6013-d424-459f-a4ef-2138780fb9bf',
			'city_id' => 'Lorem',
			'educationallevel_id' => 1,
			'salaryrange_id' => 1,
			'show_on_queries' => 1,
			'fulltext' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created' => '2014-06-16 21:22:27',
			'modified' => '2014-06-16 21:22:27'
		),
	);

}
