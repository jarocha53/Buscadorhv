busquedaCtrl = function($scope, $http){
    $scope.data = "";
    $scope.status = "";
    $scope.buscar = function(){
        method = "POST";
       
        url = "http://localhost:9090/ws_rest/ws.php"

        /*var request_data = {
                "ciudad":"",
                "nivel_educativo":"",
                "aspiracion_salarial":"",
                "busqueda":$scope.busqueda
        };*/
        var request_data = {
                "busqueda":$scope.busqueda
        };
        $http({method: method, url:  url, data:JSON.stringify(request_data)}).
            success(function(data, status) {
              $scope.status = status;
              $scope.data = data;
            }).
            error(function(data, status) {
              $scope.data = data || "Request failed";
              $scope.status = status;
          });
    }

}

var app = angular.module('main', ['ngTable']).
    controller('TableCtrl', function($scope, ngTableParams) {
        var data = [{name: "Moroni", age: 50},
                    {name: "Tiancum", age: 43},
                    {name: "Jacob", age: 27},
                    {name: "Nephi", age: 29},
                    {name: "Enos", age: 34},
                    {name: "Tiancum", age: 43},
                    {name: "Jacob", age: 27},
                    {name: "Nephi", age: 29},
                    {name: "Enos", age: 34},
                    {name: "Tiancum", age: 43},
                    {name: "Jacob", age: 27},
                    {name: "Nephi", age: 29},
                    {name: "Enos", age: 34},
                    {name: "Tiancum", age: 43},
                    {name: "Jacob", age: 27},
                    {name: "Nephi", age: 29},
                    {name: "Enos", age: 34}];

        $scope.tableParams = new ngTableParams({
            page: 1,            // show first page
            count: 10           // count per page
        }, {
            total: data.length, // length of data
            getData: function($defer, params) {
                $defer.resolve(data.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
    });

