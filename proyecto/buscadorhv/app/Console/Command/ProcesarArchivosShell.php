<?php

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeEmail', 'Network/Email');

class ProcesarArchivosShell extends AppShell{





  
  public function main(){
  
  	$niveles_educativos = array(
		"NINGUNO" => 0	,
		"PREESCOLAR" => 1	,
		"BÁSICA PRIMARIA (1° - 5°)" => 2	,
		"BÁSICA SECUNDARIA (6° - 9°)" => 3	,
		"MEDIA (10° -13°)" => 4	,
		"TÉCNICA LABORAL" => 5	,
		"TÉCNICA PROFESIONAL" => 6	,
		"TECNOLÓGICA" => 7	,
		"UNIVERSITARIA" => 8	,
		"ESPECIALIZACIÓN" => 9	,
		"POSTGRADO" => 10,
		"MAESTRÍA" => 11,
		"DOCTORADO" => 12
	);

	$files_container = WWW_ROOT.'archivos'.DS;
  
	$MUpload = ClassRegistry::init("Upload");
	$MResume = ClassRegistry::init("Resume");
	$MUploadlog = ClassRegistry::init("Uploadlog");


	$MUpload->recursive = 0;
	$uploads = $MUpload->find("all",array(
			"conditions"=>array(
				"records"=>null,
				"errors"=>null,
				"susses"=>null
			)));
	$recorre = 0;
	foreach($uploads as $key_upload => $data_upload){
		//debug($data_upload);
		$file_name =  $data_upload["Upload"]["file_name"];
		//debug($files_container.$file_name);
		
		$records = 0;
		$errors = 0;
		$susses = 0;
		$email_message = null;

		
		/*$file = new File($files_container.$file_name);
		$json = $file->read(true, 'r');*/


		$file = fopen($files_container.$file_name, "r");

		while (!feof($file)) {
			$message = null;
			$records = $records+1;
			$hv = fgets($file);
			$persona = json_decode($hv);

			switch(json_last_error()) {
		        case JSON_ERROR_NONE:
		            debug(' - Sin errores');

		        $susses = $susses+1;  

		        $city_id = $persona->persona->informacion_contacto->residencia->municipio;
		        $max_nivel_educativo = $niveles_educativos["NINGUNO"];
		        $salaryrange_id = $persona->persona->informacion_laboral->aspiracion_salarial;

		        
		        $show_on_queries = 1;
		        if(isset($persona->privacidad)){
		        	if(isset($persona->privacidad->permitir_visualizacion)){
		        		if($persona->privacidad->permitir_visualizacion != 'si'){
		        			$show_on_queries = 0;	
		        		}
		        	}
		        }

		        foreach ($persona->persona->formacion_academica as $formacion_academica_value) {
		        	$nivel_educativo = $formacion_academica_value->nivel_educativo;
		        	
		        	if($niveles_educativos[$nivel_educativo] > $max_nivel_educativo )
		        		$max_nivel_educativo = $niveles_educativos[$nivel_educativo];

		        }

		             $hojaDeVida = array(
		             	'Resume' => array(
								'city_id' => $city_id,
								'educationallevel_id' => "$max_nivel_educativo",
								'salaryrange_id' => $salaryrange_id,
								'show_on_queries' => $show_on_queries,
								'fulltext' => $hv
								));


		             	$MResume->create();
		             if ($MResume->save($hojaDeVida)) {
						debug("Guardo!!!");

					} else {
						debug("No Guardo :(");
					}

		        break;
		        case JSON_ERROR_DEPTH:
		            debug(' - Excedido tamaño máximo de la pila');
		            $errors = $errors+1;
		            $message = 'Excedido tamaño máximo de la pila';
		        break;
		        case JSON_ERROR_STATE_MISMATCH:
		            debug(' - Desbordamiento de buffer o los modos no coinciden');
		            $errors = $errors+1;
		            $message = 'Desbordamiento de buffer o los modos no coinciden';
		        break;
		        case JSON_ERROR_CTRL_CHAR:
		            debug(' - Encontrado carácter de control no esperado');
		            $errors = $errors+1;
		            $message = 'Encontrado carácter de control no esperado';
		        break;
		        case JSON_ERROR_SYNTAX:
		            debug(' - Error de sintaxis, JSON mal formado');
		            $errors = $errors+1;
		            $message = 'Error de sintaxis, JSON mal formado';
		        break;
		        case JSON_ERROR_UTF8:
		            debug(' - Caracteres UTF-8 malformados, posiblemente están mal codificados');
		            $errors = $errors+1;
		            $message = 'Caracteres UTF-8 malformados, posiblemente están mal codificados';
		        break;
		        default:
		            debug(' - Error desconocido');
		            $errors = $errors+1;
		            $message = 'Error desconocido';
		        break;
		    }

		    if(isset($message)){
		    	$email_message = "Número de linea: ".$records." - Tipo de error: ".$message."\n";
			    $data_uploadlog = array (
							'Uploadlog'=> array(
								'upload_id' => $data_upload["Upload"]["id"],
								'fulltext' => $hv,
								'row_num' => $records,
								'message' => $message
								)
							);
				$MUploadlog->create();
				if($MUploadlog->save($data_uploadlog)){
					echo "Se guardo la actualización del registro del archivo";
				}else{
					echo "No se guardo la actualización del registro del archivo";
				}
			}

		}

		$archivo = WWW_ROOT.'archivos'.DS.$file_name;
		$nuevo_archivo = WWW_ROOT.'procesados'.DS.$file_name;
		if(fclose($file)){
			if (!copy($archivo, $nuevo_archivo)) {
			    echo "Error al copiar $archivo...\n";
			}else{
				if(!unlink($archivo)){
					echo "Error al borrar el archivo $archivo...\n";
				}
			}
		}else{
			echo "No se pudo cerrar el archivo $archivo...\n";
		}

		$data_upload = array(
						'Upload' => array(
							'id' => $data_upload["Upload"]["id"],
							'records' => $records,
							'errors' => $errors,
							'susses' => $susses
							)
						);
		
		if($MUpload->save($data_upload)){
			echo "Se guardo la actualización del reistro del archivo";
		}else{
			echo "No se guardo la actualización del reistro del archivo";
		}

		debug($uploads);
		$email_user = $uploads[$recorre]["User"]["email"];

		$file_name;
		if(!isset($email_message)){
			$email_message = "Apreciado usuario, \nSe proceso el archivo ".$file_name." y no se encontraron errores";
		}else{
			$email_message = "Apreciado usuario, \nSe proceso el archivo ".$file_name." y se encontraron los siguientes errores:\n".$email_message;
		}

		$Email = new CakeEmail('smtp');
		$Email->from(array('jmedinah1@ucentral.edu.co' => 'julian medina'));
		$Email->to($email_user);
		$Email->subject('Notificación de procesamiento de archivos planos Hojas de Vida');
		$Email->send($email_message);
		$email_message = null;

		$recorre = $recorre+1;
		
	}

		
  }  
}

?>