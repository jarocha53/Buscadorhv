<style type="text/css" media="screen">
	a{
		color:red;
	}
	a:hover{
		color: #a00707;
		text-decoration: none;
	}
</style>
<div class="users index">
	<span style="color:grey;font-size: 21px;">Usuarios</span>
	<br/><br/>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<tr>
			
			<th><?php echo $this->Paginator->sort('Usuario'); ?></th>
			<th><?php echo $this->Paginator->sort('nombre'); ?></th>
			<th><?php echo $this->Paginator->sort('Primer Apellido'); ?></th>
			<th><?php echo $this->Paginator->sort('Correo Electónico'); ?></th>
			<!--th><?php //echo $this->Paginator->sort('Segundo Apellido'); ?></th-->
			<th><?php echo $this->Paginator->sort('Grupo'); ?></th>
			<th><?php echo $this->Paginator->sort('Plataforma'); ?></th>
			<!--th><?php //echo $this->Paginator->sort('Teléfono'); ?></th-->
			<!--th><?php //echo $this->Paginator->sort('Dirección'); ?></th-->
			<!--th><?php //echo $this->Paginator->sort('Creado'); ?></th-->
			<th><?php echo $this->Paginator->sort('Modificado'); ?></th>
			<th class="actions"><?php echo __('Acciones'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['nombre']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['primerApellido']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
		<!--td><?php //echo h($user['User']['segundoApellido']); ?>&nbsp;</td-->
		<td><?php echo h($user['Group']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['Platform']['name']); ?>&nbsp;</td>
		<!--td><?php //echo h($user['User']['telefonos']); ?>&nbsp;</td-->
		<!--td><?php //echo h($user['User']['direccion']); ?>&nbsp;</td-->
		<!--td><?php //echo h($user['User']['created']); ?>&nbsp;</td-->
		<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
		<td class="actions" style="text-align:center;">
			<?php echo $this->Html->link("<span style='color:red;' class='glyphicon glyphicon-search'></span>", array('action' => 'view', $user['User']['id']),array('escape' => false)); ?>

			<?php echo $this->Html->link("<span style='color:red;' class='glyphicon glyphicon-pencil'></span>", array('action' => 'edit', $user['User']['id']),array('escape' => false)); ?>

			<?php //echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php //echo $this->Form->postLink(__('Borrar'), array('action' => 'delete', $user['User']['id']), array(), __('Esta seguro de borrar el usuario %s?', $user['User']['username'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, Mostrando {:current} registros de {:count} total, iniciando en {:start} registros, finalizando en {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev("<span class='glyphicon glyphicon-arrow-left' style='margin-right: 10px;'></<span>", array('escape' => false), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next("<span class='glyphicon glyphicon-arrow-right' margin-left: 10px;></<span>", array('escape' => false), array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<!--<h3><?php echo __('Actions'); ?></h3>-->
	<ul>
		<!--<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?></li>-->
		<!--<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('List Platforms'), array('controller' => 'platforms', 'action' => 'index')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('New Platform'), array('controller' => 'platforms', 'action' => 'add')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('List Resumechecks'), array('controller' => 'resumechecks', 'action' => 'index')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('List Searches'), array('controller' => 'searches', 'action' => 'index')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('New Search'), array('controller' => 'searches', 'action' => 'add')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>-->
		<!--<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>-->
	</ul>
</div>
