<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Cambiar Contraseña'); ?></legend>
	<?php
		
		echo $this->Form->input('Contraseña Actual');
		echo $this->Form->input('password_new', array('label'=>'Nueva Contraseña',
												  'type'=>'password'));
		echo $this->Form->input('password_rewrite', array('label'=>'Confirmar Nueva Contraseña'												,'type'=>'password'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Guardar')); ?>
</div>
