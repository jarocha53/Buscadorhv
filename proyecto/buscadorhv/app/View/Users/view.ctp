<style type="text/css" media="screen">
	.margen_data{
		margin-left:10px;
	}
	.row_bottom_space{
		margin-bottom: 10px;
	}
</style>

<fieldset>
	<legend style="color:grey;">Detalle usuario</legend>
	<div class="users view">


		<div class="row row_bottom_space">
			<div class="col-md-3">
				<span style="font-weight:bold;">Nombre de Usuario</span><br/>
				<span class="margen_data"><?php echo h($user['User']['username']); ?></span>
			</div>
			<div class="col-md-3">
				<span style="font-weight:bold;">Correo Electrónico</span><br/>
				<span class="margen_data"><?php echo h($user['User']['email']); ?></span>
			</div>
			<div class="col-md-3">
				<span style="font-weight:bold;">Grupo</span><br/>
				<span class="margen_data">
					<?php 
					echo h($user['Group']['name']); 
					//echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); 
					?></span>
			</div>
			<div class="col-md-3">
				<span style="font-weight:bold;">Plataforma</span><br/>
				<span class="margen_data">
					<?php 
					echo h($user['Platform']['name']);
					//echo $this->Html->link($user['Platform']['name'], array('controller' => 'platforms', 'action' => 'view', $user['Platform']['id'])); 
					?></span>
				</label>
			</div>
		</div>
		<div class="row row_bottom_space">
			<div class="col-md-3">
				<span style="font-weight:bold;">Nombre</span><br/>
				<span class="margen_data"><?php echo h($user['User']['nombre']); ?></span>
			</div>
			<div class="col-md-3">
				<span style="font-weight:bold;">Primer Apellido</span><br/>
				<span class="margen_data"><?php echo h($user['User']['primerApellido']); ?></span>
			</div>
			<div class="col-md-3">
				<span style="font-weight:bold;">Segundo Apellido</span><br/>
				<span class="margen_data"><?php echo h($user['User']['segundoApellido']); ?></span>
			</div>
			<div class="col-md-3">
				<span style="font-weight:bold;">Teléfono</span><br/>
				<span class="margen_data"><?php echo h($user['User']['telefonos']); ?></span>
			</div>
		</div>
		<div class="row row_bottom_space">
			<div class="col-md-3">
				<span style="font-weight:bold;">Dirección</span><br/>
				<span class="margen_data"><?php echo h($user['User']['direccion']); ?></span>
			</div>                                                                              
			<div class="col-md-3">
				<span style="font-weight:bold;">Fecha Registro</span><br/>
				<span class="margen_data"><?php echo h($user['User']['created']); ?></span>
			</div>
			<div class="col-md-3">
				<span style="font-weight:bold;">Fecha Modificación</span><br/>
				<span class="margen_data"><?php echo h($user['User']['modified']); ?></span>
			</div>
		</div>



		<!--dl>
			<dt><?php //echo __('Id'); ?></dt>
			<dd>
				<?php //echo h($user['User']['id']); ?>
				&nbsp;
			</dd-->
			<!--dt><?php //echo __('Contraseña'); ?></dt>
			<dd>
				<?php //echo h($user['User']['password']); ?>
				&nbsp;
			</dd>
		</dl-->
	</div>
</fieldset>
<!--<div class="actions">
	<h3><?php //echo __('Actions'); ?></h3>
	<ul>
		<li><?php //echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php //echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php //echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Platforms'), array('controller' => 'platforms', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Platform'), array('controller' => 'platforms', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Resumechecks'), array('controller' => 'resumechecks', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Searches'), array('controller' => 'searches', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Search'), array('controller' => 'searches', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php //echo __('Related Resumechecks'); ?></h3>
	<?php ////if (!empty($user['Resumecheck'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php //echo __('Id'); ?></th>
		<th><?php //echo __('Resume Id'); ?></th>
		<th><?php //echo __('User Id'); ?></th>
		<th><?php //echo __('Send Notified'); ?></th>
		<th><?php //echo __('Created'); ?></th>
		<th><?php //echo __('Modified'); ?></th>
		<th class="actions"><?php //echo __('Actions'); ?></th>
	</tr>
	<?php //foreach ($user['Resumecheck'] as $resumecheck): ?>
		<tr>
			<td><?php //echo $resumecheck['id']; ?></td>
			<td><?php //echo $resumecheck['resume_id']; ?></td>
			<td><?php //echo $resumecheck['user_id']; ?></td>
			<td><?php //echo $resumecheck['send_notified']; ?></td>
			<td><?php //echo $resumecheck['created']; ?></td>
			<td><?php //echo $resumecheck['modified']; ?></td>
			<td class="actions">
				<?php //echo $this->Html->link(__('View'), array('controller' => 'resumechecks', 'action' => 'view', $resumecheck['id'])); ?>
				<?php //echo $this->Html->link(__('Edit'), array('controller' => 'resumechecks', 'action' => 'edit', $resumecheck['id'])); ?>
				<?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'resumechecks', 'action' => 'delete', $resumecheck['id']), array(), __('Are you sure you want to delete # %s?', $resumecheck['id'])); ?>
			</td>
		</tr>
	<?php //endforeach; ?>
	</table>
<?php //endif; ?>

	<div class="actions">
		<ul>
			<li><?php //echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php //echo __('Related Searches'); ?></h3>
	<?php //if (!empty($user['Search'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php //echo __('Id'); ?></th>
		<th><?php //echo __('User Id'); ?></th>
		<th><?php //echo __('Platform Id'); ?></th>
		<th><?php //echo __('City Id'); ?></th>
		<th><?php //echo __('Educationallevel Id'); ?></th>
		<th><?php //echo __('Salaryrange Id'); ?></th>
		<th><?php //echo __('Json Data'); ?></th>
		<th><?php //echo __('Created'); ?></th>
		<th><?php //echo __('Modified'); ?></th>
		<th><?php //echo __('Delete'); ?></th>
		<th class="actions"><?php //echo __('Actions'); ?></th>
	</tr>
	<?php //foreach ($user['Search'] as $search): ?>
		<tr>
			<td><?php //echo $search['id']; ?></td>
			<td><?php //echo $search['user_id']; ?></td>
			<td><?php //echo $search['platform_id']; ?></td>
			<td><?php //echo $search['city_id']; ?></td>
			<td><?php //echo $search['educationallevel_id']; ?></td>
			<td><?php //echo $search['salaryrange_id']; ?></td>
			<td><?php //echo $search['json_data']; ?></td>
			<td><?php //echo $search['created']; ?></td>
			<td><?php //echo $search['modified']; ?></td>
			<td><?php //echo $search['delete']; ?></td>
			<td class="actions">
				<?php //echo $this->Html->link(__('View'), array('controller' => 'searches', 'action' => 'view', $search['id'])); ?>
				<?php //echo $this->Html->link(__('Edit'), array('controller' => 'searches', 'action' => 'edit', $search['id'])); ?>
				<?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'searches', 'action' => 'delete', $search['id']), array(), __('Are you sure you want to delete # %s?', $search['id'])); ?>
			</td>
		</tr>
	<?php //endforeach; ?>
	</table>
<?php //endif; ?>

	<div class="actions">
		<ul>
			<li><?php //echo $this->Html->link(__('New Search'), array('controller' => 'searches', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php //echo __('Related Uploads'); ?></h3>
	<?php //if (!empty($user['Upload'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php //echo __('Id'); ?></th>
		<th><?php //echo __('User Id'); ?></th>
		<th><?php //echo __('Platform Id'); ?></th>
		<th><?php //echo __('File Name'); ?></th>
		<th><?php //echo __('Size'); ?></th>
		<th><?php //echo __('Records'); ?></th>
		<th><?php //echo __('Errors'); ?></th>
		<th><?php //echo __('Susses'); ?></th>
		<th><?php //echo __('Part File'); ?></th>
		<th><?php //echo __('Total Files'); ?></th>
		<th><?php //echo __('Created'); ?></th>
		<th><?php //echo __('Modified'); ?></th>
		<th class="actions"><?php //echo __('Actions'); ?></th>
	</tr>
	<?php //foreach ($user['Upload'] as $upload): ?>
		<tr>
			<td><?php //echo $upload['id']; ?></td>
			<td><?php //echo $upload['user_id']; ?></td>
			<td><?php //echo $upload['platform_id']; ?></td>
			<td><?php //echo $upload['file_name']; ?></td>
			<td><?php //echo $upload['size']; ?></td>
			<td><?php //echo $upload['records']; ?></td>
			<td><?php //echo $upload['errors']; ?></td>
			<td><?php //echo $upload['susses']; ?></td>
			<td><?php //echo $upload['part_file']; ?></td>
			<td><?php //echo $upload['total_files']; ?></td>
			<td><?php //echo $upload['created']; ?></td>
			<td><?php //echo $upload['modified']; ?></td>
			<td class="actions">
				<?php //echo $this->Html->link(__('View'), array('controller' => 'uploads', 'action' => 'view', $upload['id'])); ?>
				<?php //echo $this->Html->link(__('Edit'), array('controller' => 'uploads', 'action' => 'edit', $upload['id'])); ?>
				<?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'uploads', 'action' => 'delete', $upload['id']), array(), __('Are you sure you want to delete # %s?', $upload['id'])); ?>
			</td>
		</tr>
	<?php //endforeach; ?>
	</table>
<?php //endif; ?>

	<div class="actions">
		<ul>
			<li><?php ////echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>-->
