<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend style="color:grey;">Registrar usuario</legend>

		<div class="row">
			<div class="col-md-3">
				<label>Nombre de Usuario <br/>
					<?php echo $this->Form->input('username',array('label'=>'','class'=>'form-control')); ?>
				</label>
			</div>
			<div class="col-md-3">
				<label>Contraseña <br/>
					<?php echo $this->Form->input('password',array('label'=>'','class'=>'form-control')); ?>
				</label>
			</div>
			<div class="col-md-3">
				<label>Grupo <br/>
					<?php echo $this->Form->input('group_id',array('label'=>'','class'=>'form-control')); ?>
				</label>
			</div>
			<div class="col-md-3">
				<label>Plataforma <br/>
					<?php echo $this->Form->input('platform_id',array('label'=>'','class'=>'form-control')); ?>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label>Nombre<br/>
					<?php echo $this->Form->input('nombre',array('label'=>'','class'=>'form-control')); ?>
					
				</label>
			</div>
			<div class="col-md-3">
				<label>Primer Apellido<br/>
					<?php echo $this->Form->input('primerApellido',array('label'=>'','class'=>'form-control')); ?>
					
				</label>
			</div>
			<div class="col-md-3">
				<label>Segundo Apellido<br/>
					<?php echo $this->Form->input('segundoApellido', array('label'=>'','class'=>'form-control')); ?>
					
				</label>
			</div>
			<div class="col-md-3">
				<label>Correo Electrónico<br/>
					<?php echo $this->Form->input('email',array('label'=>'','class'=>'form-control')); ?>
					
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<label>Teléfono<br/>
					<?php echo $this->Form->input('telefonos',array('label'=>'','class'=>'form-control')); ?>
					
				</label>
			</div>
			<div class="col-md-3">
				<label>Dirección<br/>
					<?php echo $this->Form->input('direccion',array('label'=>'','class'=>'form-control')); ?>
					
				</label>
			</div>
		</div>

	
		<?php echo $this->Form->hidden('changePassword',array('value'=>'true','class'=>'form-control')); ?>
		<br/>
		<div class="row">
			<div class="col-md-3">
				<button style="width: 155px;" class="btn btn-danger"  type="submit">
                      REGISTRAR <span class="glyphicon glyphicon-floppy-disk"></span>
                    </button>
			</div>
		</div>


	</fieldset>
<?php echo $this->Form->end(__('')); ?>
</div>
<!--div class="actions">
	<h3><?php //echo __('Actions'); ?></h3>
	<ul>

		<li><?php //echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php //echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Platforms'), array('controller' => 'platforms', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Platform'), array('controller' => 'platforms', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Resumechecks'), array('controller' => 'resumechecks', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Searches'), array('controller' => 'searches', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Search'), array('controller' => 'searches', 'action' => 'add')); ?> </li>
		<li><?php //echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php //echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
	</ul>
</div-->
