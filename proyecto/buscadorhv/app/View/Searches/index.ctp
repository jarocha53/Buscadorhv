<div class="searches index">
	<h2><?php echo __('Searches'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('platform_id'); ?></th>
			<th><?php echo $this->Paginator->sort('city_id'); ?></th>
			<th><?php echo $this->Paginator->sort('educationallevel_id'); ?></th>
			<th><?php echo $this->Paginator->sort('salaryrange_id'); ?></th>
			<th><?php echo $this->Paginator->sort('json_data'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('delete'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($searches as $search): ?>
	<tr>
		<td><?php echo h($search['Search']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($search['User']['id'], array('controller' => 'users', 'action' => 'view', $search['User']['id'])); ?>
		</td>
		<td><?php echo h($search['Search']['platform_id']); ?>&nbsp;</td>
		<td><?php echo h($search['Search']['city_id']); ?>&nbsp;</td>
		<td><?php echo h($search['Search']['educationallevel_id']); ?>&nbsp;</td>
		<td><?php echo h($search['Search']['salaryrange_id']); ?>&nbsp;</td>
		<td><?php echo h($search['Search']['json_data']); ?>&nbsp;</td>
		<td><?php echo h($search['Search']['created']); ?>&nbsp;</td>
		<td><?php echo h($search['Search']['modified']); ?>&nbsp;</td>
		<td><?php echo h($search['Search']['delete']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $search['Search']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $search['Search']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $search['Search']['id']), array(), __('Are you sure you want to delete # %s?', $search['Search']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Search'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
