<div class="searches view">
<h2><?php echo __('Search'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($search['Search']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($search['User']['id'], array('controller' => 'users', 'action' => 'view', $search['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Platform Id'); ?></dt>
		<dd>
			<?php echo h($search['Search']['platform_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City Id'); ?></dt>
		<dd>
			<?php echo h($search['Search']['city_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Educationallevel Id'); ?></dt>
		<dd>
			<?php echo h($search['Search']['educationallevel_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Salaryrange Id'); ?></dt>
		<dd>
			<?php echo h($search['Search']['salaryrange_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Json Data'); ?></dt>
		<dd>
			<?php echo h($search['Search']['json_data']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($search['Search']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($search['Search']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Delete'); ?></dt>
		<dd>
			<?php echo h($search['Search']['delete']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Search'), array('action' => 'edit', $search['Search']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Search'), array('action' => 'delete', $search['Search']['id']), array(), __('Are you sure you want to delete # %s?', $search['Search']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Searches'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Search'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
