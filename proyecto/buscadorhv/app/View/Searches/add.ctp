<div class="searches form">
<?php echo $this->Form->create('Search'); ?>
	<fieldset>
		<legend><?php echo __('Add Search'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('platform_id');
		echo $this->Form->input('city_id');
		echo $this->Form->input('educationallevel_id');
		echo $this->Form->input('salaryrange_id');
		echo $this->Form->input('json_data');
		echo $this->Form->input('delete');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Searches'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
