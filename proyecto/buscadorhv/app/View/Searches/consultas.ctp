<style type="text/css" media="screen">

    .mayuscula_inicial:first-letter {
      text-transform: uppercase;
    }
    .load_data_il_estate_standby { background: #FFA8A3; color: #666666; }
    .load_data_il_estate_loading { background: yellow; }
    .load_data_il_estate_load { background: #f75a50; }

    .load_data_sena_estate_standby { background: #A9E0B1; color: #666666; }
    .load_data_sena_estate_loading { background: yellow; }
    .load_data_sena_estate_load { background: #4fde63; }

    .load_data_empleate_estate_standby { background: #B9DBFF; color: #666666; }
    .load_data_empleate_estate_loading { background: yellow; }
    .load_data_empleate_estate_load { background: #3798ff; }


    .selected_redempleo_true{ background: #FFA8A3; }
    .selected_sena_true{ background: #A9E0B1; }
    .selected_empleate_true{ background: #B9DBFF; } 

    .line_color_redempleo{ background: #FFA8A3; }
    .line_color_sena{ background: #A9E0B1; }
    .line_color_empleate{ background: #B9DBFF; }

   .boxSem{
      width: 250px;
      height: 25px;
      border: 2px solid black;
      font-weight: bold;
      color: white;
      margin: 2px;
      padding: 1px;
      padding-left: 7px;
   }
.boxenabletrue{
  background: red;
}
.boxenablefalse{
  background: green;
}
.sticky {
    padding: 0.5ex;
    width: 618px;
    background-color: #333;
    color: #fff;
    font-size: 2em;
    border-radius: 0.5ex;
}
.sticky.stick {
    position: fixed;
    top: 0;
    right: 153px;
    z-index: 10000;
    border-radius: 0 0 0.5em 0.5em;
    bottom: 89px;
    overflow-y: scroll;
    overflow-x: hidden;
}
</style>
<!--h2><?php //echo __('Buscador de Hojas de Vida'); ?></h2-->
<div ng-controller="busquedaCtrl">
    <div style="margin:auto; width:50%;  background: rgba(218, 208, 208, 0.41); text-align: center;padding-top: 2%;padding: 15px;" >
        <div ng-hide="showform" ng-click="showformAction()">
            BUSCAR DE NUEVO <span class="
glyphicon glyphicon-zoom-in"></span><br/>
            Resultados de busqueda para: {{busqueda}}
            {{estonoes}}
        </div>
        <form name="busquedaHvFrom" class="form-horizontal" ng-show="showform">
            <div class="row">
                <div class="col-md-12">
                    <textarea class="form-control" name="busqueda" rows="2" ng-model="busqueda" 
                          required ng-pattern="/^[0-9A-Za-záéíóúñÑ, ]+$/" 
                          placeholder="Perfil que desea buscar" 
                          ></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" >

                    <select class="form-control"  ng-model="nivelEducativo" style="margin-top: 10px;" required>
                      <option value="-1" selected>Nivel Educativo</option>
                      <option value="-1" selected>TODOS</option>
                      <option value="0">NINGUNO</option>
                      <option value="1">PREESCOLAR</option>
                      <option value="2">BÁSICA PRIMARIA (1° - 5°)</option>
                      <option value="3">BÁSICA SECUNDARIA (6° - 9°)</option>
                      <option value="4">MEDIA (10° -13°)</option>
                      <option value="5">TÉCNICA LABORAL</option>
                      <option value="6">TÉCNICA PROFESIONAL</option>
                      <option value="7">TECNOLÓGICA</option>
                      <option value="8">UNIVERSITARIA</option>
                      <option value="9">ESPECIALIZACIÓN</option>
                      <option value="10">POSTGRADO</option>
                      <option value="11">MAESTRÍA</option>
                      <option value="12">DOCTORADO</option>
                    </select>
                </div>
                <div class="col-md-6">
                 <select class="form-control" ng-model="rangoSalarial" style="margin-top: 10px;" required>
                    <option value="-1" selected>Rango Salarial</option>
                    <option value="-1" selected>TODOS</option>
                    <option value="1">MLV - $884.250</option>
                    <option value="2">$884.251 - $1.000.000</option>
                    <option value="3">$1.000.001 - $1.500.000</option>
                    <option value="4">$1.500.001 - $2.000.000</option>
                    <option value="5">$2.000.001 - $3.000.000</option>
                    <option value="6">$3.000.001 - $4.000.000</option>
                    <option value="7">$4.000.001 - $6.000.000</option>
                    <option value="8">$6.000.001 - $8.000.000</option>
                    <option value="9">$8.000.001 - $10.000.000</option>
                    <option value="10">$10.000.001 - $12.000.000</option>
                    <option value="11">$12.000.001 - $15.000.000</option>
                    <option value="12">Mayor de $15.000.001</option>
                    <option value="13">A Convenir</option>
                 </select>
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-6">
                    <select class="form-control" ng-model="departamento" style="margin-top: 10px;" required>
                        <option value="-1" selected>Departamento</option>
                        <option ng-repeat="departamento in departamentos" value="{{departamento.codigo}}">{{departamento.nombre}}</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <select class="form-control" ng-model="municipio" style="margin-top: 10px;" required>
                        <option value="" selected>Municipio</option>
                        <option ng-repeat="municipio in municipios  | filter: { departamento_id: departamento }" value="{{municipio.codigo}}">{{municipio.nombre}}</option>
                    </select>
                </div>    
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-md-4">                    
                      
                      <div class="panel panel-default load_data_il_estate_{{loadData.IL}}">
                        <div class="panel-body">
                          <label><input type="checkbox" ng-model="USE_IL" value="1">REDEMPLEO</label>
                        </div>
                      </div>

                </div>
                <div class="col-md-4">
                  <div class="panel panel-default load_data_sena_estate_{{loadData.Sena}}">
                        <div class="panel-body">
                          <label><input type="checkbox" ng-model="USE_SENA" value="1">SENA</label>
                        </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="panel panel-default load_data_empleate_estate_{{loadData.Empleate}}">
                        <div class="panel-body">
                          <label><input type="checkbox" ng-model="USE_EMPLEATE" value="1">EMPLEATE</label>
                        </div>
                  </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button style="font-weight:bold;width: 155px;" class="btn btn-danger" ng-disabled="busquedaHvFrom.busqueda.$invalid" ng-click="buscar()" type="button">
                      Buscar <span class="glyphicon glyphicon-search"></span>
                    </button>
                </div>
            </div>
        </form>
    
    </div>
    <hr>
    <!--div class="boxSem boxenable{{semaforo}}"></div-->

 
   <div>
      
      <div class="row">
        <div class="col-md-5">

              <div class="row selected_{{valuePersona.plataforma}}_{{valuePersona.selected}}" ng-repeat="(keyPersona, valuePersona) in personas.data track by $index" ng-click="seleccionar_persona(keyPersona,valuePersona)" style="border:1px solid gray;cursor:pointer/*padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                      <div class="col-md-1">
                        <div class="line_color_{{valuePersona.plataforma}}" style="width: 77%;height: 10%;margin: 4px;margin-left: -8px;">
                          
                        </div>
                      </div>
                      <div class="col-md-11">
                        <div id="nombre_apellido" style="font-weight:bold;">
                            {{valuePersona.persona.informacion_personal.nombre}} 
                            {{valuePersona.persona.informacion_personal.primer_apellido}} 
                            <!--{{valuePersona.persona.informacion_personal.segundo_apellido}} -->

                            <!--[{{valuePersona.plataforma}}]-->

                            
                        </div>
                        <div id="lugar_residencia">
                            {{valuePersona.persona.informacion_contacto.residencia.departamento}} 
                            {{valuePersona.persona.informacion_contacto.residencia.municipio}}
                        </div>
                        <div id="profesiones">
                            | <span ng-repeat="(keyFormacion, valueFormacion) in valuePersona.persona.formacion_academica"> 
                                {{valueFormacion.titulo}} |
                            </span>
                        </div>  
                      </div>
              </div>

        </div>
        <div id="sticky-anchor"></div>
        <div class="col-md-7" scroll ng-show="$parent.displaypersona" style="background: rgba(218, 208, 208, 0.31);font-size: 14px;color:#333333;    overflow-y: scroll;overflow-x: hidden;">
            <!-- width: 56%;height: 400px;right: 5px;position: fixed;   -->
            
              <span style="font-size:20px;font-weight:bold;">{{$parent.persona.persona.informacion_personal.nombre}} {{$parent.persona.persona.informacion_personal.primer_apellido}}</span><br/><br/>
              <div class="row">
                <div class="col-md-6">
                <b>Edad:</b>  {{$parent.persona.persona.informacion_personal.edad}} años
                </div>
                <div class="col-md-6"><b>Dirección:</b> {{$parent.persona.persona.informacion_contacto.residencia.direccion}}</div>
              </div>
              <div class="row">
                <div class="col-md-6"><b>Telefono:</b> {{$parent.persona.persona.informacion_contacto.telefono[0].numero}}</div>
                <div class="col-md-6"><b>Email:</b> {{$parent.persona.persona.informacion_contacto.correo_electronico}}</div>
              </div>
              <br/>
              <div class="row">
                <div class="col-md-12" style="font-size:18px;font-weight:bold;">Perfil Laboral</div>
              </div>
              <div class="row">
                <div class="col-md-12" style="text-align: justify; padding-left: 26px; padding-right: 26px; letter-spacing: 0;">{{$parent.persona.persona.informacion_laboral.descripcion_perfil_laboral}}</div>
              </div>
              <br/>
              <div class="row">
                <div class="col-md-12" style="font-size:18px;font-weight:bold;">Formación Académica</div>
              </div>
              <div class="row" ng-repeat="(keyFormacion, valueFormacion) in $parent.persona.persona.formacion_academica">
                  <div class="col-md-8">
                    <span style="font-weight:bold;">Titulo:</span> <span class="mayuscula_inicial">{{valueFormacion.titulo}}</span>
                  </div>
                  <div class="col-md-4">
                    <span class="mayuscula_inicial">{{valueFormacion.estado}}</span>
                  </div>
              </div>
              <br/>
              <div class="row">
                <div class="col-md-12" style="font-size:18px;font-weight:bold;">Experiencia Laboral</div>
              </div>
              <div class="row" ng-repeat="(keyFormacion, valueExperiencia) in $parent.persona.persona.experiencia_laboral">
                  <div class="col-md-8">
                    <span style="font-weight:bold;">Cargo:</span> <span class="mayuscula_inicial">{{valueExperiencia.cargo}}</span>
                  </div>
                  <div class="col-md-4">
                    <span>{{valueExperiencia.fecha_ingreso}}</span> -
                    <span>{{valueExperiencia.fecha_finalizacion}}</span>
                  </div>
              </div>

              
              
            
        </div>

      </div>


    </div>
  </div>