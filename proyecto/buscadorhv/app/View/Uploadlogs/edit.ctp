<div class="uploadlogs form">
<?php echo $this->Form->create('Uploadlog'); ?>
	<fieldset>
		<legend><?php echo __('Edit Uploadlog'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('upload_id');
		echo $this->Form->input('fulltext');
		echo $this->Form->input('row_num');
		echo $this->Form->input('message');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Uploadlog.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Uploadlog.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Uploadlogs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
	</ul>
</div>
