<div class="uploadlogs view">
<h2><?php echo __('Uploadlog'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($uploadlog['Uploadlog']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Upload'); ?></dt>
		<dd>
			<?php echo $this->Html->link($uploadlog['Upload']['id'], array('controller' => 'uploads', 'action' => 'view', $uploadlog['Upload']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fulltext'); ?></dt>
		<dd>
			<?php echo h($uploadlog['Uploadlog']['fulltext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Row Num'); ?></dt>
		<dd>
			<?php echo h($uploadlog['Uploadlog']['row_num']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Message'); ?></dt>
		<dd>
			<?php echo h($uploadlog['Uploadlog']['message']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($uploadlog['Uploadlog']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($uploadlog['Uploadlog']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Uploadlog'), array('action' => 'edit', $uploadlog['Uploadlog']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Uploadlog'), array('action' => 'delete', $uploadlog['Uploadlog']['id']), array(), __('Are you sure you want to delete # %s?', $uploadlog['Uploadlog']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploadlogs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uploadlog'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
	</ul>
</div>
