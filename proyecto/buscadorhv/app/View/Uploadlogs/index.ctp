<div class="uploadlogs index">
	<h2><?php echo __('Uploadlogs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('upload_id'); ?></th>
			<th><?php echo $this->Paginator->sort('fulltext'); ?></th>
			<th><?php echo $this->Paginator->sort('row_num'); ?></th>
			<th><?php echo $this->Paginator->sort('message'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($uploadlogs as $uploadlog): ?>
	<tr>
		<td><?php echo h($uploadlog['Uploadlog']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($uploadlog['Upload']['id'], array('controller' => 'uploads', 'action' => 'view', $uploadlog['Upload']['id'])); ?>
		</td>
		<td><?php echo h($uploadlog['Uploadlog']['fulltext']); ?>&nbsp;</td>
		<td><?php echo h($uploadlog['Uploadlog']['row_num']); ?>&nbsp;</td>
		<td><?php echo h($uploadlog['Uploadlog']['message']); ?>&nbsp;</td>
		<td><?php echo h($uploadlog['Uploadlog']['created']); ?>&nbsp;</td>
		<td><?php echo h($uploadlog['Uploadlog']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $uploadlog['Uploadlog']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $uploadlog['Uploadlog']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $uploadlog['Uploadlog']['id']), array(), __('Are you sure you want to delete # %s?', $uploadlog['Uploadlog']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Uploadlog'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
	</ul>
</div>
