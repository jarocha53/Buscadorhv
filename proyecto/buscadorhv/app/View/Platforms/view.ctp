<div class="platforms view">
<h2><?php echo __('Platform'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Url Ws'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['url_ws']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Hashpass'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['hashpass']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Internal Data'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['internal_data']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email Messages'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['email_messages']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Enable'); ?></dt>
		<dd>
			<?php echo h($platform['Platform']['enable']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Platform'), array('action' => 'edit', $platform['Platform']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Platform'), array('action' => 'delete', $platform['Platform']['id']), array(), __('Are you sure you want to delete # %s?', $platform['Platform']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Platforms'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Platform'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Searches'), array('controller' => 'searches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Search'), array('controller' => 'searches', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Searches'); ?></h3>
	<?php if (!empty($platform['Search'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Platform Id'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Educationallevel Id'); ?></th>
		<th><?php echo __('Salaryrange Id'); ?></th>
		<th><?php echo __('Json Data'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Delete'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($platform['Search'] as $search): ?>
		<tr>
			<td><?php echo $search['id']; ?></td>
			<td><?php echo $search['user_id']; ?></td>
			<td><?php echo $search['platform_id']; ?></td>
			<td><?php echo $search['city_id']; ?></td>
			<td><?php echo $search['educationallevel_id']; ?></td>
			<td><?php echo $search['salaryrange_id']; ?></td>
			<td><?php echo $search['json_data']; ?></td>
			<td><?php echo $search['created']; ?></td>
			<td><?php echo $search['modified']; ?></td>
			<td><?php echo $search['delete']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'searches', 'action' => 'view', $search['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'searches', 'action' => 'edit', $search['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'searches', 'action' => 'delete', $search['id']), array(), __('Are you sure you want to delete # %s?', $search['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Search'), array('controller' => 'searches', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Uploads'); ?></h3>
	<?php if (!empty($platform['Upload'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Platform Id'); ?></th>
		<th><?php echo __('File Name'); ?></th>
		<th><?php echo __('Size'); ?></th>
		<th><?php echo __('Records'); ?></th>
		<th><?php echo __('Errors'); ?></th>
		<th><?php echo __('Susses'); ?></th>
		<th><?php echo __('Part File'); ?></th>
		<th><?php echo __('Total Files'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($platform['Upload'] as $upload): ?>
		<tr>
			<td><?php echo $upload['id']; ?></td>
			<td><?php echo $upload['user_id']; ?></td>
			<td><?php echo $upload['platform_id']; ?></td>
			<td><?php echo $upload['file_name']; ?></td>
			<td><?php echo $upload['size']; ?></td>
			<td><?php echo $upload['records']; ?></td>
			<td><?php echo $upload['errors']; ?></td>
			<td><?php echo $upload['susses']; ?></td>
			<td><?php echo $upload['part_file']; ?></td>
			<td><?php echo $upload['total_files']; ?></td>
			<td><?php echo $upload['created']; ?></td>
			<td><?php echo $upload['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'uploads', 'action' => 'view', $upload['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'uploads', 'action' => 'edit', $upload['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'uploads', 'action' => 'delete', $upload['id']), array(), __('Are you sure you want to delete # %s?', $upload['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($platform['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('Platform Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Nombre'); ?></th>
		<th><?php echo __('PrimerApellido'); ?></th>
		<th><?php echo __('SegundoApellido'); ?></th>
		<th><?php echo __('Telefonos'); ?></th>
		<th><?php echo __('Direccion'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($platform['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['group_id']; ?></td>
			<td><?php echo $user['platform_id']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td><?php echo $user['nombre']; ?></td>
			<td><?php echo $user['primerApellido']; ?></td>
			<td><?php echo $user['segundoApellido']; ?></td>
			<td><?php echo $user['telefonos']; ?></td>
			<td><?php echo $user['direccion']; ?></td>
			<td><?php echo $user['created']; ?></td>
			<td><?php echo $user['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), array(), __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
