<div class="platforms index">
	<h2><?php echo __('Platforms'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('url_ws'); ?></th>
			<th><?php echo $this->Paginator->sort('hashpass'); ?></th>
			<th><?php echo $this->Paginator->sort('internal_data'); ?></th>
			<th><?php echo $this->Paginator->sort('email_messages'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('enable'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($platforms as $platform): ?>
	<tr>
		<td><?php echo h($platform['Platform']['id']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['name']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['url_ws']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['hashpass']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['internal_data']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['email_messages']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['created']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['modified']); ?>&nbsp;</td>
		<td><?php echo h($platform['Platform']['enable']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $platform['Platform']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $platform['Platform']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $platform['Platform']['id']), array(), __('Are you sure you want to delete # %s?', $platform['Platform']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Platform'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Searches'), array('controller' => 'searches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Search'), array('controller' => 'searches', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('controller' => 'uploads', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
