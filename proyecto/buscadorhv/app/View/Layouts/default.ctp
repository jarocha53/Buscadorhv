<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('BuscadorHV', 'Buscador de Hojas de Vida - SPE');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())

?>
<!DOCTYPE html lang="es">
<html xmlns:ng="http://angularjs.org" >
<head>
	<?php echo $this->Html->charset(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<link data-require="bootstrap-css@*" data-semver="3.0.0" rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
    
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    
	<script data-require="angular.js@1.1.x" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.1.5/angular.js" data-semver="1.1.5"></script>
	
    <script data-require="ui-bootstrap@0.5.0" data-semver="0.5.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.6.0.js"></script>
    
    <style type="text/css">
    
        body{
            background-image: url(<?php echo $this->webroot; ?>img/style/fondo.png);
            background-repeat: no-repeat;
            background-position-x: 100%;
            background-size: 32%;
            background-position-y: 54px;
        }
        
        #footer{
            position: fixed;
            bottom: 0px;
            left: 0px;
            right: 0px;
            height: 90px;
            background-color: white;
        }
        
        #sombra{
            background-image: url(<?php echo $this->webroot; ?>img/style/background_footer.png);
            /* height: 61px; */
            bottom: 0px;
            /* background: red; */
            /*position: fixed;*/
            right: 0px;
            left: 0px;
            background-size: contain;
            height: 30px;
        }
        
        #uaespe{
            float: left;
            height: 10%;
            width: 50%;
            padding-left: 10%;
            color: #666666;
            font-size: 13px;
            padding-top: 10px;
        }
        
        #img_oprtunidades_trabajo{
            background-image: url(<?php echo $this->webroot; ?>img/style/oprtunidades_trabajo.png);
            height: 60%;
            background-repeat: no-repeat;
            right: 0px;
            background-size: contain;
            float: left;
            width: 50%;
            background-position-x: 70%;
        }
        
    </style>
	<?php
		echo $this->Html->meta('icon');
		
  		//echo $this->Html->script('angular.min', array('block' => 'script'));
  		//echo $this->Html->script('angular-touch', array('block' => 'script'));
  		echo $this->Html->script('buscadorhv.logic', array('block' => 'script'));
  		echo $this->Html->script('ng-table', array('block' => 'script'));
  		//echo $this->Html->script('ui-bootstrap-tpls-0.11.0.min', array('block' => 'script'));
  		
		//echo $this->Html->css('cake.generic');
		echo $this->Html->css('ng-table');
		//echo $this->Html->css('bootstrap.min');
		//echo $this->Html->css('bootstrap-theme.min');
		echo $this->fetch('meta');
		echo $this->fetch('css');


		echo $this->fetch('script');
	?>
</head>
<body ng-app="buscadorHV">
	<div id="container">
		
			
			<nav class="navbar navbar-default" role="navigation" ng-controller="ctrlHeader">
		      <!-- Brand and toggle get grouped for better mobile display -->
		      <div class="navbar-header">

		        <button type="button" class="navbar-toggle" ng-init="navCollapsed = true" ng-click="navCollapsed = !navCollapsed">
		          <span class="sr-only">Toggle navigation</span>
		          <span class="icon-bar"></span>
		          <span class="icon-bar"></span>
		          <span class="icon-bar"></span>
		        </button>
		        <a class="navbar-brand" href="#">
                    <!-- Brand -->
                    <?php echo $this->Html->image('style/spe.png',array('style'=>'width: 40%;')); ?>
                    <!--img src="../img/style/spe.png" style="width: 40%;" /-->
                </a>
		      </div>
		    
		      <!-- Collect the nav links, forms, and other content for toggling -->
		      <div class="collapse navbar-collapse" ng-class="!navCollapsed && 'in'" ng-click="navCollapsed=true">
		      
		        <ul class="nav navbar-nav">
		         <?php if($userData['Group']['name'] == 'administrator'){ ?>
		          	  <li>
			        	<?php echo $this->Html->link('Buscador',array('controller' => 'Searches', 'action' => 'consultas')); ?>
			          </li>
			          <li>
			          	<a href="#" class="dropdown-toggle" ng-controller="DropdownCtrl">Administrar usuarios <b class="caret"></b></a>
			        	<ul class="dropdown-menu">
			        	  <li><?php echo $this->Html->link('Listar usuarios',array('controller' => 'Users', 'action' => 'index')); ?></li>
			              <li><?php echo $this->Html->link('Registrar usuario', array('controller' => 'Users', 'action' => 'add')); ?></li>
			            </ul>
			          </li>
			          <li>
		        		<a href="#" class="dropdown-toggle" ng-controller="DropdownCtrl">Administrar HV <b class="caret"></b></a>
			        	<ul class="dropdown-menu">
			        	  <li><?php echo $this->Html->link('Listar archivos cargados',array('controller' => 'Uploads', 'action' => 'index')); ?></li>
			              <li><?php echo $this->Html->link('Cargar Hojas de Vida', array('controller' => 'Uploads', 'action' => 'add')); ?></li>
			            </ul>
		          	  </li>
		         <?php }elseif($userData['Group']['name'] == 'administrator' || $userData['Group']['name'] == 'user platform'){ ?>
		          	<li>
			        	<?php echo $this->Html->link('Buscador',array('controller' => 'Searches', 'action' => 'consultas')); ?>
			        </li>
		          	<li>
		          		<a href="#" class="dropdown-toggle" ng-controller="DropdownCtrl">Administrar HV <b class="caret"></b></a>
			        	<ul class="dropdown-menu">
			        	  <li><?php echo $this->Html->link('Listar archivos cargados',array('controller' => 'Uploads', 'action' => 'index')); ?></li>
			              <li><?php echo $this->Html->link('Cargar Hojas de Vida', array('controller' => 'Uploads', 'action' => 'add')); ?></li>
			            </ul>
		          	</li>
		          <?php } ?>
		          <!--<li class="active"><a href="#">Link</a></li>-->
		        
		          <!--<li><a href="#">Link</a></li>-->
		          
		          <!--<li class="dropdown">
		            <a href="#" class="dropdown-toggle" ng-controller="DropdownCtrl">Dropdown <b class="caret"></b></a>
		            <ul class="dropdown-menu">
		              <li><a href="#">Action</a></li>
		              <li><a href="#">Another action</a></li>
		              <li><a href="#">Something else here</a></li>
		              <li><a href="#">Separated link</a></li>
		              <li><a href="#">One more separated link</a></li>
		            </ul>
		          </li>-->
		          
		        </ul>
		        <!--<form class="navbar-form navbar-left" role="search">
		          <div class="form-group">
		            <input type="text" class="form-control" placeholder="Search">
		          </div>
		          <button type="submit" class="btn btn-default">Submit</button>
		        </form>-->
		        <ul class="nav navbar-nav navbar-right">
				<?php if($userData['Group']['name'] == 'administrator' || $userData['Group']['name'] == 'user platform' || $userData['Group']['name'] == 'user'){ ?>
		          <li>
					<a href="#" class="dropdown-toggle" ng-controller="DropdownCtrl"><?php echo $userData['nombre'].' '.$userData['primerApellido'] ?> <b class="caret"></b></a>
					<ul class="dropdown-menu">
					  <li><?php echo $this->Html->link('Cambio de contraseña',array('controller' => 'Users', 'action' => 'changePassword')); ?></li>
					</ul>
				  </li>
				  <?php } ?>
				<?php if($userData['Group']['name'] == 'administrator' || $userData['Group']['name'] == 'user platform' || $userData['Group']['name'] == 'user'){ ?>
		          <li><?php echo $this->Html->link('Cerrar sesión',array('controller' => 'Users', 'action' => 'logout')); ?>
				  <?php } ?>
		          </li>
		          <!--<li class="dropdown">
		            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
		            <ul class="dropdown-menu">
		              <li><a href="#">Action</a></li>
		              <li><a href="#">Another action</a></li>
		              <li><a href="#">Something else here</a></li>
		              <li><a href="#">Separated link</a></li>
		            </ul>
		          </li>-->
		        </ul>
		      </div><!-- /.navbar-collapse -->
		    </nav>

	</div>
		<div class="container theme-showcase" id="content">

			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</div>
    
		<div id="footer">
            <div id="sombra"></div>
            
                <div id="uaespe">
                    2014.<br/>Unidad Administrativa Especial del Servicio Público de Empleo.
                </div>
                <div id="img_oprtunidades_trabajo"></div>
       
			<?php //echo $this->Html->link(
					//$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
					//'http://www.cakephp.org/',
					//array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
				//);
			?>
			<!--p>
				<?php //echo $cakeVersion; ?>
			</p-->
		</div>
	</div>
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
