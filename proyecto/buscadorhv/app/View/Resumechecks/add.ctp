<div class="resumechecks form">
<?php echo $this->Form->create('Resumecheck'); ?>
	<fieldset>
		<legend><?php echo __('Add Resumecheck'); ?></legend>
	<?php
		echo $this->Form->input('resume_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('send_notified');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Resumechecks'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Resumes'), array('controller' => 'resumes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resume'), array('controller' => 'resumes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
