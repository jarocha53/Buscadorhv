<div class="resumechecks view">
<h2><?php echo __('Resumecheck'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resumecheck['Resumecheck']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Resume'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resumecheck['Resume']['id'], array('controller' => 'resumes', 'action' => 'view', $resumecheck['Resume']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resumecheck['User']['id'], array('controller' => 'users', 'action' => 'view', $resumecheck['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Send Notified'); ?></dt>
		<dd>
			<?php echo h($resumecheck['Resumecheck']['send_notified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resumecheck['Resumecheck']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resumecheck['Resumecheck']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resumecheck'), array('action' => 'edit', $resumecheck['Resumecheck']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resumecheck'), array('action' => 'delete', $resumecheck['Resumecheck']['id']), array(), __('Are you sure you want to delete # %s?', $resumecheck['Resumecheck']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Resumechecks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resumecheck'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resumes'), array('controller' => 'resumes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resume'), array('controller' => 'resumes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
