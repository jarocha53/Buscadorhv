<div class="resumes view">
<h2><?php echo __('Resume'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resume['City']['name'], array('controller' => 'cities', 'action' => 'view', $resume['City']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Educationallevel'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resume['Educationallevel']['name'], array('controller' => 'educationallevels', 'action' => 'view', $resume['Educationallevel']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Salaryrange'); ?></dt>
		<dd>
			<?php echo $this->Html->link($resume['Salaryrange']['title'], array('controller' => 'salaryranges', 'action' => 'view', $resume['Salaryrange']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Show On Queries'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['show_on_queries']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fulltext'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['fulltext']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($resume['Resume']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Resume'), array('action' => 'edit', $resume['Resume']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Resume'), array('action' => 'delete', $resume['Resume']['id']), array(), __('Are you sure you want to delete # %s?', $resume['Resume']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Resumes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resume'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Educationallevels'), array('controller' => 'educationallevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Educationallevel'), array('controller' => 'educationallevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salaryranges'), array('controller' => 'salaryranges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salaryrange'), array('controller' => 'salaryranges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resumechecks'), array('controller' => 'resumechecks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Resumechecks'); ?></h3>
	<?php if (!empty($resume['Resumecheck'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Resume Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Send Notified'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($resume['Resumecheck'] as $resumecheck): ?>
		<tr>
			<td><?php echo $resumecheck['id']; ?></td>
			<td><?php echo $resumecheck['resume_id']; ?></td>
			<td><?php echo $resumecheck['user_id']; ?></td>
			<td><?php echo $resumecheck['send_notified']; ?></td>
			<td><?php echo $resumecheck['created']; ?></td>
			<td><?php echo $resumecheck['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'resumechecks', 'action' => 'view', $resumecheck['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'resumechecks', 'action' => 'edit', $resumecheck['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'resumechecks', 'action' => 'delete', $resumecheck['id']), array(), __('Are you sure you want to delete # %s?', $resumecheck['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
