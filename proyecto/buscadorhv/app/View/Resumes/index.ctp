<div class="resumes index">
	<h2><?php echo __('Resumes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('city_id'); ?></th>
			<th><?php echo $this->Paginator->sort('educationallevel_id'); ?></th>
			<th><?php echo $this->Paginator->sort('salaryrange_id'); ?></th>
			<th><?php echo $this->Paginator->sort('show_on_queries'); ?></th>
			<th><?php echo $this->Paginator->sort('fulltext'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($resumes as $resume): ?>
	<tr>
		<td><?php echo h($resume['Resume']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($resume['City']['name'], array('controller' => 'cities', 'action' => 'view', $resume['City']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($resume['Educationallevel']['name'], array('controller' => 'educationallevels', 'action' => 'view', $resume['Educationallevel']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($resume['Salaryrange']['title'], array('controller' => 'salaryranges', 'action' => 'view', $resume['Salaryrange']['id'])); ?>
		</td>
		<td><?php echo h($resume['Resume']['show_on_queries']); ?>&nbsp;</td>
		<td><?php echo h($resume['Resume']['fulltext']); ?>&nbsp;</td>
		<td><?php echo h($resume['Resume']['created']); ?>&nbsp;</td>
		<td><?php echo h($resume['Resume']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $resume['Resume']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $resume['Resume']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $resume['Resume']['id']), array(), __('Are you sure you want to delete # %s?', $resume['Resume']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Resume'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Educationallevels'), array('controller' => 'educationallevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Educationallevel'), array('controller' => 'educationallevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salaryranges'), array('controller' => 'salaryranges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salaryrange'), array('controller' => 'salaryranges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resumechecks'), array('controller' => 'resumechecks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>
	</ul>
</div>
