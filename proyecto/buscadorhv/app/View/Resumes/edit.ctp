<div class="resumes form">
<?php echo $this->Form->create('Resume'); ?>
	<fieldset>
		<legend><?php echo __('Edit Resume'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('city_id');
		echo $this->Form->input('educationallevel_id');
		echo $this->Form->input('salaryrange_id');
		echo $this->Form->input('show_on_queries');
		echo $this->Form->input('fulltext');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Resume.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Resume.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Resumes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Educationallevels'), array('controller' => 'educationallevels', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Educationallevel'), array('controller' => 'educationallevels', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Salaryranges'), array('controller' => 'salaryranges', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Salaryrange'), array('controller' => 'salaryranges', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Resumechecks'), array('controller' => 'resumechecks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Resumecheck'), array('controller' => 'resumechecks', 'action' => 'add')); ?> </li>
	</ul>
</div>
