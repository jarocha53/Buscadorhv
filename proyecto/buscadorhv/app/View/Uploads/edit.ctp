<div class="uploads form">
<?php echo $this->Form->create('Upload'); ?>
	<fieldset>
		<legend><?php echo __('Edit Upload'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('platform_id');
		echo $this->Form->input('file_name');
		echo $this->Form->input('size');
		echo $this->Form->input('records');
		echo $this->Form->input('errors');
		echo $this->Form->input('susses');
		echo $this->Form->input('part_file');
		echo $this->Form->input('total_files');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<!--<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Upload.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Upload.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Platforms'), array('controller' => 'platforms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Platform'), array('controller' => 'platforms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploadlogs'), array('controller' => 'uploadlogs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uploadlog'), array('controller' => 'uploadlogs', 'action' => 'add')); ?> </li>
	</ul>
</div>
-->