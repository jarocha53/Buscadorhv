<div class="uploads view">
<h2><?php echo __('Upload'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($upload['User']['id'], array('controller' => 'users', 'action' => 'view', $upload['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Platform'); ?></dt>
		<dd>
			<?php echo $this->Html->link($upload['Platform']['name'], array('controller' => 'platforms', 'action' => 'view', $upload['Platform']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File Name'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['file_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Size'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Records'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['records']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Errors'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['errors']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Susses'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['susses']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Part File'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['part_file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Files'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['total_files']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($upload['Upload']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<!--<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Upload'), array('action' => 'edit', $upload['Upload']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Upload'), array('action' => 'delete', $upload['Upload']['id']), array(), __('Are you sure you want to delete # %s?', $upload['Upload']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploads'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Upload'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Platforms'), array('controller' => 'platforms', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Platform'), array('controller' => 'platforms', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Uploadlogs'), array('controller' => 'uploadlogs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Uploadlog'), array('controller' => 'uploadlogs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Uploadlogs'); ?></h3>
	<?php if (!empty($upload['Uploadlog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Upload Id'); ?></th>
		<th><?php echo __('Fulltext'); ?></th>
		<th><?php echo __('Row Num'); ?></th>
		<th><?php echo __('Message'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($upload['Uploadlog'] as $uploadlog): ?>
		<tr>
			<td><?php echo $uploadlog['id']; ?></td>
			<td><?php echo $uploadlog['upload_id']; ?></td>
			<td><?php echo $uploadlog['fulltext']; ?></td>
			<td><?php echo $uploadlog['row_num']; ?></td>
			<td><?php echo $uploadlog['message']; ?></td>
			<td><?php echo $uploadlog['created']; ?></td>
			<td><?php echo $uploadlog['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'uploadlogs', 'action' => 'view', $uploadlog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'uploadlogs', 'action' => 'edit', $uploadlog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'uploadlogs', 'action' => 'delete', $uploadlog['id']), array(), __('Are you sure you want to delete # %s?', $uploadlog['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Uploadlog'), array('controller' => 'uploadlogs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>-->
