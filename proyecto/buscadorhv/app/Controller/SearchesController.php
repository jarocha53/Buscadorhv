<?php
App::uses('AppController', 'Controller');
/**
 * Searches Controller
 *
 * @property Search $Search
 * @property PaginatorComponent $Paginator
 */
class SearchesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Search->recursive = 0;
		$this->set('searches', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Search->exists($id)) {
			throw new NotFoundException(__('Invalid search'));
		}
		$options = array('conditions' => array('Search.' . $this->Search->primaryKey => $id));
		$this->set('search', $this->Search->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Search->create();
			if ($this->Search->save($this->request->data)) {
				$this->Session->setFlash(__('The search has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The search could not be saved. Please, try again.'));
			}
		}
		$users = $this->Search->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Search->exists($id)) {
			throw new NotFoundException(__('Invalid search'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Search->save($this->request->data)) {
				$this->Session->setFlash(__('The search has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The search could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Search.' . $this->Search->primaryKey => $id));
			$this->request->data = $this->Search->find('first', $options);
		}
		$users = $this->Search->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function delete($id = null) {
		$this->Search->id = $id;
		if (!$this->Search->exists()) {
			throw new NotFoundException(__('Invalid search'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Search->delete()) {
			$this->Session->setFlash(__('The search has been deleted.'));
		} else {
			$this->Session->setFlash(__('The search could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function json_busqueda($platfrom_id){
		$this->layout = "ajax";
		$this->uses = array("Resume","Search");
		$json = file_get_contents('php://input');

if($platfrom_id == 2){
	sleep (2);
}

		$requestData = json_decode($json);
		//debug($requestData);

		/* object(stdClass) {
			consulta => 'Ingeniero de sistemas'
			salario => '1'
			NivelEducativo => '3'
		} */

		

		$query = $requestData->busqueda;
		$salaryRange = $requestData->aspiracion_salarial;
		$educationLevel = $requestData->nivel_educativo;
		$city = $requestData->ciudad;

		$query = trim($query);

		$query = preg_replace('/\s+/', ' ', $query);

		if ($query == null || strlen($query) == 0)
		{
		    $query = "";
		}

		$query = preg_replace("/[\=|\+|\-|\*|\?|\&|\<|\>|\,|'|\(|\)|\[|\]|\{|\}|\-|_|\~]+/", "",$query);
		$query = preg_replace('/"/', ' ',$query);
		$query = trim($query);

		$preposiciones = array(	"ante"," ","cabe","con","contra","de","desde","durante","en","entre","hacia","hasta","mediante","para","por","según","segun","sin","sobre","tras","versus","es");
		$articulos = array("el", "lo", "su", "a", "sobre", "de", "los", "las", "suyo", "su", "este", "aquél", "aquel", "ese", "mi", "tu", "nuestro", "vuestro");
		$adverbios = array("ahora","antes","después","despues","tarde","luego","ayer","temprano","ya","todavía","todavia","anteayer","aún","aun","pronto","hoy","aquí","aqui","ahí","ahi","allí","alli","cerca","lejos","fuera","dentro","alrededor","aparte","encima","debajo","delante","detrás","detras","así","asi","bien","mal","despacio","deprisa","como	","mucho","poco","muy","casi","todo","nada","algo","medio","demasiado","bastante","más","mas","menos","además","ademas","incluso","sí","si","también","tambien","asimismo","no","tampoco","jamás","jamas","nunca","acaso","quizá","quiza","tal","vez","a","lo","mejor");
		$palabrasrechazadas = array("AND", "OR", "and", "or", "y", "Y", "O", "o");

		foreach ($preposiciones as $preposicion_key => $preposicion_value) {
			$query = preg_replace("/ ".$preposicion_value." /", ' ',$query);
			$query = preg_replace("/^".$preposicion_value." /", ' ',$query);
			$query = preg_replace("/ ".$preposicion_value."$/", ' ',$query);
		}

		foreach ($articulos as $articulos_key => $articulos_value) {
			$query = preg_replace("/ ".$articulos_value." /", ' ',$query);
			$query = preg_replace("/^".$articulos_value." /", ' ',$query);
			$query = preg_replace("/ ".$articulos_value."$/", ' ',$query);
		}

		foreach ($adverbios as $adverbios_key => $adverbios_value) {
			$query = preg_replace("/ ".$adverbios_value." /", ' ',$query);
			$query = preg_replace("/^".$adverbios_value." /", ' ',$query);
			$query = preg_replace("/ ".$adverbios_value."$/", ' ',$query);
		}

		foreach ($palabrasrechazadas as $palabrasrechazadas_key => $palabrasrechazadas_value) {
			$query = preg_replace("/ ".$palabrasrechazadas_value." /", '',$query);
			$query = preg_replace("/^".$palabrasrechazadas_value." /", '',$query);
			$query = preg_replace("/ ".$palabrasrechazadas_value."$/", '',$query);
		}

		$query = trim($query);
		$query = preg_replace('/\s+/', ' ', $query);
		if ($query == null || strlen($query) == 0 || $query == " " ){
		     $query = "";
		}


		$fulltext_conditions = array("platform_id"=>$platfrom_id);
		$query_list = explode(" ", $query);
		foreach($query_list as $key => $value)
			$fulltext_conditions[] = "MATCH (Resume.`fulltext`) AGAINST ('$value')";

		if($salaryRange != -1)
			$fulltext_conditions[] = "Resume.salaryrange_id = '$salaryRange'";

		if($educationLevel != -1)
			$fulltext_conditions[] = "Resume.educationallevel_id = '$educationLevel'";

		/**
		* "00000" or "" 
		*/
		if($city  != "" and $city != "00000" )
			$fulltext_conditions[] = "Resume.city_id = '$city'";


		$this->Resume->recursive = -1;
		$json = $this->Resume->find("all",
			array(
					"fields"=>array("fulltext"),
					"conditions"=>$fulltext_conditions
			));

		$result = "";
		$cantidad = count($json);
		$contador = 0;
		foreach($json as $key => $value){
			$contador = $contador + 1;
			foreach ($value["Resume"] as $key2 => $value2) {
				if($contador == $cantidad){
					$result = $result.$value2;
				}else{
					$result = $result.$value2.",";	
				}
			}
		}


		$this->set("json","[".$result."]");

	}
	function consultas(){}


}
