<?php
App::uses('AppController', 'Controller');
/**
 * Resumechecks Controller
 *
 * @property Resumecheck $Resumecheck
 * @property PaginatorComponent $Paginator
 */
class ResumechecksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Resumecheck->recursive = 0;
		$this->set('resumechecks', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Resumecheck->exists($id)) {
			throw new NotFoundException(__('Invalid resumecheck'));
		}
		$options = array('conditions' => array('Resumecheck.' . $this->Resumecheck->primaryKey => $id));
		$this->set('resumecheck', $this->Resumecheck->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Resumecheck->create();
			if ($this->Resumecheck->save($this->request->data)) {
				$this->Session->setFlash(__('The resumecheck has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The resumecheck could not be saved. Please, try again.'));
			}
		}
		$resumes = $this->Resumecheck->Resume->find('list');
		$users = $this->Resumecheck->User->find('list');
		$this->set(compact('resumes', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Resumecheck->exists($id)) {
			throw new NotFoundException(__('Invalid resumecheck'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Resumecheck->save($this->request->data)) {
				$this->Session->setFlash(__('The resumecheck has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The resumecheck could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Resumecheck.' . $this->Resumecheck->primaryKey => $id));
			$this->request->data = $this->Resumecheck->find('first', $options);
		}
		$resumes = $this->Resumecheck->Resume->find('list');
		$users = $this->Resumecheck->User->find('list');
		$this->set(compact('resumes', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Resumecheck->id = $id;
		if (!$this->Resumecheck->exists()) {
			throw new NotFoundException(__('Invalid resumecheck'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Resumecheck->delete()) {
			$this->Session->setFlash(__('The resumecheck has been deleted.'));
		} else {
			$this->Session->setFlash(__('The resumecheck could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
