<?php
App::uses('AppController', 'Controller');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
App::uses('CakeEmail', 'Network/Email');
/**
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


public function beforeFilter() {
    parent::beforeFilter();
    // Allow users to register and logout.
    $this->Auth->allow('add', 'logout');
}



/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuario Invalido'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('El usuario ha sido guardado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El usuario no pudo ser guardado, por favor intentelo de nuevo.'));
			}
		}
		$groups = $this->User->Group->find('list');
		$platforms = $this->User->Platform->find('list');
		$this->set(compact('groups', 'platforms'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuario invalido'));
		}
		//debug($this->User);
		if ($this->request->is(array('post', 'put'))) {
			$userToEdit = $this->request->data;
			$sendMail = false;
			if(empty($userToEdit["User"]["password"]))
				unset($userToEdit["User"]["password"]);
			else{
				$userToEdit["User"]["changePassword"] = true;
				$sendMail = true;
			}
			
			if ($this->User->save($userToEdit)) {
				if($sendMail){
					$email_user = $userToEdit["User"]["email"];

					$message = "Apreciado usuario, \n\nSu contraseña ha sido cambiada, para ingresar al sistema su usuario es el ";
					$message = $message." mismo y su nueva contraseña es: ".$userToEdit["User"]["password"]." \nTenga en cuenta que la proxima vez que valla a ";
					$message = $message." ingresar a la aplicacion el sistema le pedira que cambie su contraseña";

					$Email = new CakeEmail('smtp');
					$Email->from(array("noresponder@serviciodeempleo.gov.co" => ''));
					$Email->to($email_user);
					$Email->subject('Cambio de contraseña');
					$Email->send($message);
					$email_message = null;
				}
				$this->Session->setFlash(__('El usuario ha sido guarado.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('El usuario no pudo ser guardado, por favor intentelo de nuevo.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$userToEdit = $this->User->find('first', $options);
			$userToEdit["User"]["password"] = "";
			$this->request->data = $userToEdit;
		}
		$groups = $this->User->Group->find('list');
		$platforms = $this->User->Platform->find('list');
		$this->set(compact('groups', 'platforms'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Usuario invalido.'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('El usuario ha sido borrado.'));
		} else {
			$this->Session->setFlash(__('El usuario no pudo ser borrado, por favor intentelo de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function login() {
	    if ($this->request->is('post')) {

	        if ($this->Auth->login()) {
	            return $this->redirect($this->Auth->redirect());
	        }
	        $this->Session->setFlash(__('Invalido nombre de usuario o contraseña, por favor vuelva a intetar'));
	    }
	}

	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}	

	public function changePassword(){
		
		$usuario = $this->Auth->user('id');

		if ($this->request->is(array('post', 'put'))) {
			$this->User->recursive = 0;
			$data = $this->User->find("first",array(
			    							'conditions' => array(
			    								"User.id"=>$usuario)));

			$passwordHasher = new SimplePasswordHasher();
			$old = $new = $this->request->data["User"]["password"];
			$new = $this->request->data["User"]["password_new"];
			$rewrite = $this->request->data["User"]["password_rewrite"];
			
			$encripted = $passwordHasher->hash($old);
			if($data["User"]["password"] == $encripted){
				if($old != $new){
					if($new == $rewrite){
						$data1 = array(
									'User' => array(
										'id' => $usuario,
										'password' => $new,
										'changePassword' => false
										)
									);	
						if ($this->User->save($data1)) {
							//$this->Session->setFlash(__('The password has been modified.'));
							return $this->redirect(array('controller' => 'Users', 'action' => 'logout'));
						} else {
							$this->Session->setFlash(__('El password no puede ser modificado, por favor vuelva a intetar'));
						}
					}else{
						$this->Session->setFlash(__('La verificacion del password es diferente'));		
					}
				}else{
					$this->Session->setFlash(__('El password debe ser diferente'));	
				}
			}else{
				$this->Session->setFlash(__('El password no es el mismo que tiene actualmente el usuario'));
			}
		}

	}

}
