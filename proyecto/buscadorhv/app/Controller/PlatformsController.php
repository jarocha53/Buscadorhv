<?php
App::uses('AppController', 'Controller');
/**
 * Platforms Controller
 *
 * @property Platform $Platform
 * @property PaginatorComponent $Paginator
 */
class PlatformsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Platform->recursive = 0;
		$this->set('platforms', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Platform->exists($id)) {
			throw new NotFoundException(__('Invalid platform'));
		}
		$options = array('conditions' => array('Platform.' . $this->Platform->primaryKey => $id));
		$this->set('platform', $this->Platform->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Platform->create();
			if ($this->Platform->save($this->request->data)) {
				$this->Session->setFlash(__('The platform has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The platform could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Platform->exists($id)) {
			throw new NotFoundException(__('Invalid platform'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Platform->save($this->request->data)) {
				$this->Session->setFlash(__('The platform has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The platform could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Platform.' . $this->Platform->primaryKey => $id));
			$this->request->data = $this->Platform->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Platform->id = $id;
		if (!$this->Platform->exists()) {
			throw new NotFoundException(__('Invalid platform'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Platform->delete()) {
			$this->Session->setFlash(__('The platform has been deleted.'));
		} else {
			$this->Session->setFlash(__('The platform could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
