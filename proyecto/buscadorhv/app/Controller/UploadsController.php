<?php
App::uses('AppController', 'Controller');
/**
 * Uploads Controller
 *
 * @property Upload $Upload
 * @property PaginatorComponent $Paginator
 */
class UploadsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Upload->recursive = 0;
		$user = $this->Auth->User();
		$platform_id = $user["Platform"]["id"];

		$this->Paginator->settings = array(
	        'conditions' => array('Upload.platform_id' => $platform_id),
	        'limit' => 10
	    );
	    $uploads = $this->Paginator->paginate('Upload');
	    $this->set(compact('uploads'));
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Upload->exists($id)) {
			throw new NotFoundException(__('Invalid upload'));
		}
		$options = array('conditions' => array('Upload.' . $this->Upload->primaryKey => $id));
		$this->set('upload', $this->Upload->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Upload->create();
			//$this->Auth->User()
			//debug($this->request->data);
			if( $this->request->data['Upload']['file_name']['error'] == 0 &&  $this->request->data['Upload']['file_name']['size'] > 0){
				
				
				$archivo = $this->request->data["Upload"]["file_name"];
				$destino = WWW_ROOT.'archivos'.DS;
				
				
				
				if((move_uploaded_file($archivo['tmp_name'], $destino.$archivo['name']))){
				
				$user = $this->Auth->User();

				
				$user_id = $user["id"];
				$platform_id = $user["Platform"]["id"];
				$file_name = $this->request->data["Upload"]["file_name"]["name"];
				$size = $this->request->data["Upload"]["file_name"]["size"];
				
				$split_part = split("\.",$file_name);

				$file_name_parts = split("_",$split_part[0]);
				
				if(isset($file_name_parts[0]) && isset($file_name_parts[1]) && isset($file_name_parts[2])){
				
						/*array(
							(int) 0 => '18062014',	//fecha de generacion del archivo
							(int) 1 => '1',			// numero del archivo ($part_file)
							(int) 2 => '1'			// cantidad de archivos ($total_files)
						)*/
			
						$part_file = $file_name_parts[1];
						$total_files = $file_name_parts[2];
			
					
							$data_upload = array(
												'Upload' => array(
													'file_name' => $file_name,
													'user_id' => $user_id,
													'platform_id' => $platform_id,
													'size' => $size,
													'part_file' => $part_file,
													'total_files' => $total_files
													)
												);
												
							if($this->Upload->save($data_upload)){
								$this->Session->setFlash(__('Se guardo la información'));
								return $this->redirect(array('action' => 'index'));
							}else{
								$this->Session->setFlash(__('No se guardo la información'));
							}
					}
					else{
						$this->Session->setFlash(__('El nombre de archivo no es correcto'));
					}					
				}else{
					$this->Session->setFlash(__('Error al mover el archivo'));
				}
			}else{
				$this->Session->setFlash(__('Error al intentar subir el archivo'));
			}
			
		}
		$users = $this->Upload->User->find('list');
		$platforms = $this->Upload->Platform->find('list');
		$this->set(compact('users', 'platforms'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Upload->exists($id)) {
			throw new NotFoundException(__('Invalid upload'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Upload->save($this->request->data)) {
				$this->Session->setFlash(__('The upload has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The upload could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Upload.' . $this->Upload->primaryKey => $id));
			$this->request->data = $this->Upload->find('first', $options);
		}
		$users = $this->Upload->User->find('list');
		$platforms = $this->Upload->Platform->find('list');
		$this->set(compact('users', 'platforms'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Upload->id = $id;
		$archivo = $this->request->data["Upload"]["file_name"];
		$destino = WWW_ROOT.'archivos'.DS;

		$data = $this->Upload->find('first',array('conditions'=>array('Upload.id'=>$id)));

		$file_name = $data['Upload']['file_name'];
		debug($destino.$file_name);

		if (!$this->Upload->exists()) {
			throw new NotFoundException(__('Nombre de archivo invalido'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Upload->delete()) {
			
			unlink($destino.$file_name);
			/*if(unlink($destino.$file_name))
				$this->Session->setFlash(__('El archivo ha sido borrado.'));
			else
				$this->Session->setFlash(__('El archivo no ha sido borrado.'));*/
			$this->Session->setFlash(__('El archivo ha sido borrado.'));
		} else {
			$this->Session->setFlash(__('El archivo no pudo ser borrado, por favor pruebe de nuevo.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
}
