<?php
App::uses('AppController', 'Controller');
/**
 * Uploadlogs Controller
 *
 * @property Uploadlog $Uploadlog
 * @property PaginatorComponent $Paginator
 */
class UploadlogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Uploadlog->recursive = 0;
		$this->set('uploadlogs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Uploadlog->exists($id)) {
			throw new NotFoundException(__('Invalid uploadlog'));
		}
		$options = array('conditions' => array('Uploadlog.' . $this->Uploadlog->primaryKey => $id));
		$this->set('uploadlog', $this->Uploadlog->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Uploadlog->create();
			if ($this->Uploadlog->save($this->request->data)) {
				$this->Session->setFlash(__('The uploadlog has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The uploadlog could not be saved. Please, try again.'));
			}
		}
		$uploads = $this->Uploadlog->Upload->find('list');
		$this->set(compact('uploads'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Uploadlog->exists($id)) {
			throw new NotFoundException(__('Invalid uploadlog'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Uploadlog->save($this->request->data)) {
				$this->Session->setFlash(__('The uploadlog has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The uploadlog could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Uploadlog.' . $this->Uploadlog->primaryKey => $id));
			$this->request->data = $this->Uploadlog->find('first', $options);
		}
		$uploads = $this->Uploadlog->Upload->find('list');
		$this->set(compact('uploads'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Uploadlog->id = $id;
		if (!$this->Uploadlog->exists()) {
			throw new NotFoundException(__('Invalid uploadlog'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Uploadlog->delete()) {
			$this->Session->setFlash(__('The uploadlog has been deleted.'));
		} else {
			$this->Session->setFlash(__('The uploadlog could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
