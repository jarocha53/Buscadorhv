<?php
App::uses('AppModel', 'Model');
/**
 * Resume Model
 *
 * @property City $City
 * @property Educationallevel $Educationallevel
 * @property Salaryrange $Salaryrange
 * @property Resumecheck $Resumecheck
 */
class Resume extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Educationallevel' => array(
			'className' => 'Educationallevel',
			'foreignKey' => 'educationallevel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Salaryrange' => array(
			'className' => 'Salaryrange',
			'foreignKey' => 'salaryrange_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Resumecheck' => array(
			'className' => 'Resumecheck',
			'foreignKey' => 'resume_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
