<?php
App::uses('AppModel', 'Model');
/**
 * Search Model
 *
 * @property User $User
 * @property Platform $Platform
 * @property City $City
 * @property Educationallevel $Educationallevel
 * @property Salaryrange $Salaryrange
 */
class Search extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'json_data' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Platform' => array(
			'className' => 'Platform',
			'foreignKey' => 'platform_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Educationallevel' => array(
			'className' => 'Educationallevel',
			'foreignKey' => 'educationallevel_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Salaryrange' => array(
			'className' => 'Salaryrange',
			'foreignKey' => 'salaryrange_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
